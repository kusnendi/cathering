<?php
/**
* Created by kusnendi.muhamad
* 18-04-2016
*/
class FeaturedComposer {

	public function compose($view)
	{
		$view->with('items', Product::GetFeatured());
	}
}