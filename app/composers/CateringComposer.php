<?php

/**
* Created by kusnendi.muhamad
* 18-04-2016
*/
class CateringComposer {

	public function compose($view)
	{
		$view->with('carbo', Product::GetByCategory(3))
			->with('meat', Product::GetByCategory(4))
			->with('veggie', Product::GetByCategory(5))
			->with('sides', Product::GetByCategory(6))
			->with('dessert', Product::GetByCategory(7));
	}
}