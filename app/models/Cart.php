<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Cart extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'carts';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    /*protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];*/

    /**
     * Get Products Relation
     */
    public function Product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

    /**
     * Get Subtotal Carts
     */
    public static function Subtotal($session_id)
    {
        return DB::table('carts')->select(DB::raw("sum(amount*price) as subtotal"))
            ->where('session_id', $session_id)
            ->first();
    }
}