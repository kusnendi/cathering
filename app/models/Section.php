<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Section extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'sections';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    protected $fillable = ['section','seo_name','meta_description','meta_keyword','page_title','status'];
}