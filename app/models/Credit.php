<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/25/2016
 * Time: 5:07 PM
 */

class Credit extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'user_credits';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

}