<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Status extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'statuses';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    protected $fillable = [
        'status',
        'type',
        'description'
    ];
}