<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    /**
     * Guarded Field
     */
    protected $guarded = ['id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


    /**
     * set for Mutators
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Array type users
     */
    public static function types()
    {
        return array(
            0   => 'Administrator',
            9   => 'Customer'
        );
    }

    /**
     * User Status
     */
    public static function status()
    {
        return array(
            0   => 'New',
            1   => 'Active',
            2   => 'Disable',
            3   => 'Blocked'
        );
    }

    /**
     * Get the password for the user
     * @return string
     */
    public function password()
    {
        return $this->password;
    }

    /**
     * Get User Profile
     */
    public function profile()
    {
        return $this->hasOne('UserProfile');
    }

    public static function fullname($id)
    {
        $profile = UserProfile::find($id);
        return $profile->fullname;
    }

    public static function points()
    {
        return Credit::whereUserId(Auth::id())->first();
    }

}
