<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class OrderConfirmation extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'order_confirmations';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Touch parent relation timestamps
     */
    protected $touches = array('order');

    /**
     * set Mutators
     */
    public function setToAttribute($value)
    {
        $this->attributes['to'] = strtoupper($value);
    }
    public function setSenderAttribute($value)
    {
        $this->attributes['sender'] = strtoupper($value);
    }

    /**
     * Approved Status
     */
    public static function approved()
    {
        return array(
            0   => ['name' => 'Waiting Confirm', 'label' => 'label-warning'],
            1   => ['name' => 'Verified', 'label' => 'label-success'],
            2   => ['name' => 'Not Found', 'label' => 'label-warning']
        );
    }


    /**
     * Belongs to Order
     */
    public function order()
    {
        return $this->belongsTo('Order');
    }

    /**
     * Attributes fillable
     */
    /*protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];*/
}