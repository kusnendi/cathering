<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Category extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'categories';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];
}