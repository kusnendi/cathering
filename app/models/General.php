<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/28/2016
 * Time: 9:45 PM
 */

class General {

    /**
     * @return array
     * return common status
     */
    public static function status()
    {
        $status = [
            'Active',
            'Disable'
        ];

        return $status;
    }

    /**
     * @param $section
     * @return array common actions
     */
    public static function actions($section)
    {

        $actions = array(
            [
                'name'  => 'lihat',
                'action'=> "admin/" . $section . "/view"
            ],
            [
                'name'  => 'edit',
                'action'=> "admin/" . $section . "/edit"
            ],
            [
                'name'  => 'hapus',
                'action'=> "admin/" . $section . "/delete"
            ]
        );

        return $actions;

    }

    public static function label()
    {

    }
}