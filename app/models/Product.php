<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Product extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'products';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * @array status
     */
    public static function status()
    {
        return array(
            0 => 'disable',
            1 => 'active'
        );
    }

    /**
     * belongs to category
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

    /**
    * Get Product by Category
    * return @array
    */
    public static function GetByCategory($param)
    {
        return self::where('category_id', $param)
            ->get();
    }

    /**
    * Get Featured Product
    * return @array
    */
    public static function GetFeatured()
    {
        return self::where('featured', 1)
            ->get();
    }

    /**
     * Attributes fillable
     */
    /*protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];*/
}