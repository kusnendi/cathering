<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/16/2016
 * Time: 12:56 AM
 */

class UserProfile extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_profiles';

    /**
     * Guarded Field
     */
    protected $guarded = ['id'];
}