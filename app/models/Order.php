<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class Order extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'orders';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Order Statuses
     */
    public static function status()
    {
        return array(
            'O' => ['name' => 'Baru', 'btn' => 'btn-warning'],
            'P' => ['name' => 'Proses', 'btn' => 'btn-info'],
            'S' => ['name' => 'Pengiriman', 'btn' => 'btn-primary'],
            'D' => ['name' => 'Selesai', 'btn' => 'btn-success'],
            'C' => ['name' => 'Dibatalkan', 'btn' => 'btn-danger'],
            'W' => ['name' => 'Menunggu', 'btn' => 'btn-warning'],
            'K' => ['name' => 'Menunggu', 'btn' => 'btn-warning']
        );
    }

    /**
     * Order has one
     * Order Confirmation
     */
    public function confirmation()
    {
        return $this->hasOne('OrderConfirmation');
    }

    /**
     * Order Details
     */
    public function details()
    {
        return $this->hasMany('OrderDetail');
    }

    /**
     * User Profile
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Gift Voucher
     */
    public function coupons()
    {
        return $this->hasOne('GiftCertHistory');
    }
}