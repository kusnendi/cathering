<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class GiftCert extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'gift_certificates';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    /*protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];*/

    /**
     * Voucher Status
     */
    public static function status()
    {
        return array(
            'P' => 'Pending',
            'U' => 'Used',
            'A' => 'Active',
            'D' => 'Disabled'
        );
    }

    /**
     * Gift Cert Logs
     */
    public function logs()
    {
        return $this->hasMany('GiftCertHistory','gift_cert_id','id');
    }
}