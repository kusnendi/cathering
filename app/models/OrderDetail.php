<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 1/24/2016
 * Time: 10:43 PM
 */

class OrderDetail extends Eloquent {

    /**
     * Table used by these model
     */
    protected $table = 'order_details';

    /**
     * Guarded id from mass fill
     */
    protected $guarded = ['id'];

    /**
     * Attributes fillable
     */
    /*protected $fillable = [
        'parent_id',
        'category',
        'seo_name',
        'meta_description',
        'meta_keyword',
        'page_title',
        'status',
        'section_id'
    ];*/
}