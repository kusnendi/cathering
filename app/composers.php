<?php

/* 
 * @creator kusnendi.muhamad
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

View::composers(array(
    'UserCredits'   => ['block.account.mypoints','block.menu'],
    'CateringComposer'	=> ['web.index.components.food-category'],
    'FeaturedComposer'	=> ['block.carousel.swiper']
));