<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/25/2016
 * Time: 2:38 PM
 */

/**
 * Auth Events
 */
Event::listen('auth.*', function($param)
{
    if(Event::firing() == 'auth.onLogin')
    {
        $param->last_login = time();
        $param->save();

        return false;
    }
    elseif(Event::firing() == 'auth.onLogout')
    {

        Session::forget('event');
        Session::forget('area');
        /*if (Session::has('sid')) Session::forget('sid');*/

        Auth::logout();

        return false;
    }
});