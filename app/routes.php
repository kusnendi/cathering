<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Filtering Auth User */
Route::group(['before' => 'auth'], function()
{
    Route::controller('accounts', 'AccountController');
    Route::controller('orders', 'OrderController');
});

Route::group(array('prefix' => 'admin', 'before' => 'admin'), function ()
{
    Route::controller('dashboard', 'DashboardController');
    Route::controller('orders', 'OrderController');
    Route::controller('users', 'UsersController');
    Route::controller('vouchers', 'GiftCertsController');
    Route::controller('products', 'ProductsController');
});

Route::controller('oauth', 'OauthController');
Route::controller('ajax', 'AjaxController');
Route::controller('voucher', 'GiftCertsController');
Route::controller('carts', 'CartsController');
Route::controller('password', 'RemindersController');
Route::controller('/', 'WebController');