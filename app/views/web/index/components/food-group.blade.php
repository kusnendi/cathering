<div class="food-body food-swiper">
	<div class="swiper-wrapper">
        @foreach($carbo as $item)
            <div class="swiper-slide food-card">
                <img src="{{asset($item->image)}}" alt="...">
                <div class="food-card-body">
                    <div class="title">{{str_limit($item->product,20)}}</div>
                    <div class="food-meta">
                        <div class="food-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="food-point">
                            {{$item->point}} Point
                        </div>
                    </div>
                </div>
                <div class="food-card-head">
                    <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                </div>
            </div>
        @endforeach
	</div>
</div>

<div class="food-body food-swiper">
    <div class="swiper-wrapper">
        @foreach($meat as $item)
            <div class="swiper-slide food-card">
                <img src="{{asset($item->image)}}" alt="...">
                <div class="food-card-body">
                    <div class="title">{{str_limit($item->product,20)}}</div>
                    <div class="food-meta">
                        <div class="food-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="food-point">
                            {{$item->point}} Point
                        </div>
                    </div>
                </div>
                <div class="food-card-head">
                    <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="food-body food-swiper">
    <div class="swiper-wrapper">
        @foreach($veggie as $item)
            <div class="swiper-slide food-card">
                <img src="{{asset($item->image)}}" alt="...">
                <div class="food-card-body">
                    <div class="title">{{str_limit($item->product,20)}}</div>
                    <div class="food-meta">
                        <div class="food-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="food-point">
                            {{$item->point}} Point
                        </div>
                    </div>
                </div>
                <div class="food-card-head">
                    <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="food-body food-swiper">
    <div class="swiper-wrapper">
        @foreach($sides as $item)
            <div class="swiper-slide food-card">
                <img src="{{asset($item->image)}}" alt="...">
                <div class="food-card-body">
                    <div class="title">{{str_limit($item->product,20)}}</div>
                    <div class="food-meta">
                        <div class="food-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="food-point">
                            {{$item->point}} Point
                        </div>
                    </div>
                </div>
                <div class="food-card-head">
                    <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="food-body food-swiper">
    <div class="swiper-wrapper">
        @foreach($dessert as $item)
            <div class="swiper-slide food-card">
                <img src="{{asset($item->image)}}" alt="...">
                <div class="food-card-body">
                    <div class="title">{{str_limit($item->product,20)}}</div>
                    <div class="food-meta">
                        <div class="food-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="food-point">
                            {{$item->point}} Point
                        </div>
                    </div>
                </div>
                <div class="food-card-head">
                    <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                </div>
            </div>
        @endforeach
    </div>
</div>