<section class="food-categories">
	<div class="container">
		<div class="row">
		
			<div class="col-md-9">
				<div class="food-group">
					<div class="food-head">
						<div class="title">Carbo</div>
					</div>
					{{--*/ $cat = 'carbo' /*--}}
                    <div class="food-body food-swiper">
                        <div class="swiper-wrapper">
                            @foreach($carbo as $item)
                                <div class="swiper-slide food-card">
                                    <img src="{{asset($item->image)}}" alt="...">
                                    <div class="food-card-body">
                                        <div class="title">{{str_limit($item->product,20)}}</div>
                                        <div class="food-meta">
                                            <div class="food-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="food-point">
                                                {{$item->point}} Point
                                            </div>
                                        </div>
                                    </div>
                                    <div class="food-card-head">
                                        <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
				</div>

				<div class="food-group">
					<div class="food-head">
						<div class="title">Meat</div>
					</div>
					{{--*/ $cat = 'meat' /*--}}
                    <div class="food-body food-swiper">
                        <div class="swiper-wrapper">
                            @foreach($meat as $item)
                                <div class="swiper-slide food-card">
                                    <img src="{{asset($item->image)}}" alt="...">
                                    <div class="food-card-body">
                                        <div class="title">{{str_limit($item->product,20)}}</div>
                                        <div class="food-meta">
                                            <div class="food-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="food-point">
                                                {{$item->point}} Point
                                            </div>
                                        </div>
                                    </div>
                                    <div class="food-card-head">
                                        <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
				</div>

				<div class="food-group">
					<div class="food-head">
						<div class="title">Veggie</div>
					</div>
					{{--*/ $cat = 'veggie' /*--}}
                    <div class="food-body food-swiper">
                        <div class="swiper-wrapper">
                            @foreach($veggie as $item)
                                <div class="swiper-slide food-card">
                                    <img src="{{asset($item->image)}}" alt="...">
                                    <div class="food-card-body">
                                        <div class="title">{{str_limit($item->product,20)}}</div>
                                        <div class="food-meta">
                                            <div class="food-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="food-point">
                                                {{$item->point}} Point
                                            </div>
                                        </div>
                                    </div>
                                    <div class="food-card-head">
                                        <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
				</div>

				<div class="food-group">
					<div class="food-head">
						<div class="title">Sides</div>
					</div>
					{{--*/ $cat = 'sides' /*--}}
                    <div class="food-body food-swiper">
                        <div class="swiper-wrapper">
                            @foreach($sides as $item)
                                <div class="swiper-slide food-card">
                                    <img src="{{asset($item->image)}}" alt="...">
                                    <div class="food-card-body">
                                        <div class="title">{{str_limit($item->product,20)}}</div>
                                        <div class="food-meta">
                                            <div class="food-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="food-point">
                                                {{$item->point}} Point
                                            </div>
                                        </div>
                                    </div>
                                    <div class="food-card-head">
                                        <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
				</div>

				<div class="food-group">
					<div class="food-head">
						<div class="title">Dessert</div>
					</div>
					{{--*/ $cat = 'dessert' /*--}}
                    <div class="food-body food-swiper">
                        <div class="swiper-wrapper">
                            @foreach($dessert as $item)
                                <div class="swiper-slide food-card">
                                    <img src="{{asset($item->image)}}" alt="...">
                                    <div class="food-card-body">
                                        <div class="title">{{str_limit($item->product,20)}}</div>
                                        <div class="food-meta">
                                            <div class="food-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                            <div class="food-point">
                                                {{$item->point}} Point
                                            </div>
                                        </div>
                                    </div>
                                    <div class="food-card-head">
                                        <a href="{{url('ajax/add-to-lunch-box')}}" class="ajax-add-item" data-item="{{$item->id}}" data-method="post">Add to Lunch Box</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
				</div>
			</div>

			<div class="col-md-3">
				
				<div class="widget-box lunch-box-wrapper">
					<h2>Launch Box</h2>
					<div class="lunch-box">
						<ol class="item-lunch-box">
							<li>
								<div>Nasi Putih</div>
								<div>0 Point</div>
							</li>
							<li>
								<div>Ayam Goreng Lengkuas</div>
								<div>2 Point</div>
							</li>
							<li>
								<div>Pakcoy Cah Bawang Putih</div>
								<div>1 Point</div>
							</li>
							<li>
								<div>Puding Nutella</div>
								<div>1 Point</div>
							</li>
						</ol>

						<div class="text-center" style="padding-top: 20px">
							<a href="#" style="font-size: 13px;">Save Lunch Box</a>
						</div>
					</div>

					<div class="item">
						<div class="label-title">Sub Total Point</div>
						<div class="value">4 Point</div>
					</div>				
				</div>

			</div>

		</div>
	</div>
</section>