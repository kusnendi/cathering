<section class="delicious-menu">
    <div class="container">
        <figure>
            <img src="assets/public/images/delicious-menu.png" alt="">
        </figure>
        <div class="figcaption">
            <h3>experience our delicious menu!</h3>
            <a class="button white-btn" href="{{url('gallery')}}">view now</a>
        </div>
    </div>
</section>