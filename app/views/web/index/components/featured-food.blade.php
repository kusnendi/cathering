<div class="row">
    <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
            <img src="{{asset('products/1.jpg')}}" alt="...">
            <div class="caption">
                <h3>Food Special 1</h3>
                <div class="food-meta">
                    <div class="food-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="food-point">
                        1 Point
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
            <img src="{{asset('products/2.jpg')}}" alt="...">
            <div class="caption">
                <h3>Food Special 2</h3>
                <div class="food-meta">
                    <div class="food-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                    </div>
                    <div class="food-point">
                        1 Point
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
            <img src="{{asset('products/3.jpg')}}" alt="...">
            <div class="caption">
                <h3>Food Special 3</h3>
                <div class="food-meta">
                    <div class="food-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="food-point">
                        1 Point
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
            <img src="{{asset('products/4.jpg')}}" alt="...">
            <div class="caption">
                <h3>Food Special 4</h3>
                <div class="food-meta">
                    <div class="food-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="food-point">
                        1 Point
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>