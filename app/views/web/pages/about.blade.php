@extends('layouts.default')

@section('content')
    <div class="our-gallery">
        <section class="banner">
            <div class="container">
                <div class="banner-caption">
                    <h2>About Us</h2>
                    <h5>EXPERIENCE OUR DELICIOUS MENU!</h5>
                </div>
            </div>
        </section>

        <section class="main-content" style="padding-top: 30px">
            <div class="container">
                <article class="content-wrapper clearfix">
                    <div class="clearfix event-gallery-images">
                        <div class="main-event-img-wrapper">

                            <a class="main-event-img imgLiquidFill imgLiquid fancybox" href="{{asset('assets/public/images/index-bg.jpg')}}" data-fancybox-group="gallery">
                                <img src="{{asset('assets/public/images/index-bg.jpg')}}" alt="">

                                <div class="label">
                                    <h4>Cadillac diner</h4>
                                </div>
                            </a>

                        </div>

                        <div class="side-event-img-wrapper">
                            <div class="clearfix side-event-images">
                                <div class="img-wrapper">

                                    <a class="imgLiquidFill imgLiquid fancybox" href="{{asset('assets/public/images/search-food-item2.jpg')}}" data-fancybox-group="gallery">
                                        <img src="{{asset('assets/public/images/search-food-item2.jpg')}}" alt="">
                                    </a>

                                </div>
                                <div class="img-wrapper">

                                    <a class="imgLiquidFill imgLiquid fancybox" href="{{asset('assets/public/images/search-food-item8.jpg')}}" data-fancybox-group="gallery">
                                        <img src="{{asset('assets/public/images/search-food-item8.jpg')}}" alt="">
                                    </a>

                                </div>
                                <div class="img-wrapper">

                                    <a class="imgLiquidFill imgLiquid fancybox" href="http://localhost:8000/assets/public/images/search-food-item6.jpg" data-fancybox-group="gallery">
                                        <img src="http://localhost:8000/assets/public/images/search-food-item6.jpg" alt="">
                                    </a>

                                </div>
                                <div class="img-wrapper">

                                    <a class="imgLiquidFill imgLiquid fancybox" href="{{asset('assets/public/images/search-food-item4.jpg')}}" data-fancybox-group="gallery">
                                        <img src="{{asset('assets/public/images/search-food-item4.jpg')}}" alt="">
                                    </a>

                                </div>
                            </div>
                        </div> <!-- SIDE-EVENT-IMG-WRAPPER ends -->
                    </div> <!-- EVENT-GALLERY-IMAGES ends -->


                    <div class="video-gallery">
                        <h3>video gallery</h3>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                        <div class="imgLiquidFill imgLiquid" >
                            <img src="{{asset('assets/public/images/featured-video.jpg')}}" alt="">

                            <div class="video-caption">
                                <h2>jhone doe's</h2>
                                <h3>live concerts 2014</h3>
                                <h6>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet</h6>
                            </div>

                            <div class="label-btn clearfix">
                                <h4>featured video</h4>
                                <a href="#"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>


                    <div class="in-house clearfix">
                        <h3>in-house event hosted</h3>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/index-bg.jpg')}}" alt="">
                            </div>
                            <h4>Mardi Gras theme</h4>
                            <p>Event details and other details</p>
                        </div>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/index2-bg.jpg')}}" alt="">
                            </div>
                            <h4>Stars in your eyes</h4>
                            <p>Event details and other details</p>
                        </div>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/banner-img.jpg')}}" alt="">
                            </div>
                            <h4>Military Balls</h4>
                            <p>Event details and other details</p>
                        </div>

                    </div> <!-- IN-HOUSE ends -->


                    <div class="own-event clearfix">
                        <h3>own event hosted</h3>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/index-bg.jpg')}}" alt="">
                            </div>
                            <h4>Underwater party</h4>
                            <p>Event details and other details</p>
                        </div>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/index2-bg.jpg')}}" alt="">
                            </div>
                            <h4>Winter Wonderland</h4>
                            <p>Event details and other details</p>
                        </div>

                        <div class="img-wrappers">
                            <div class="imgLiquidFill imgLiquid">
                                <img src="{{asset('assets/public/images/banner-img.jpg')}}" alt="">
                            </div>
                            <h4>Wild West</h4>
                            <p>Event details and other details</p>
                        </div>

                    </div> <!-- OWN-EVENT ends -->

                </article> <!-- CONTENT-WRAPPER ends -->
            </div> <!-- CONTAINER ends -->
        </section> <!-- MAIN CONTENT ends -->
    </div>
@stop