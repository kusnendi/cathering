@extends('layouts.default')

@section('content')
    <div class="contact-page">
        <section class="banner">
            <div class="container">
                <div class="banner-caption">
                    <h2>Contact Us</h2>
                    <h5>Silakan isi form dibawah ini untuk menghubungi kami, Kami akan membalas pesan Anda dalam waktu 1x24 jam.</h5>
                </div>
            </div>
        </section>

        <section class="main-content">
            <div class="container">

                <img src="{{asset('assets/public/images/contact-map.png')}}" alt="">
                <article class="content-wrapper">
                    <img src="{{asset('assets/public/images/contact-map.png')}}" alt="">

                    <div class="logo-box" >
                        <img src="{{asset('assets/public/images/logo3.png')}}" alt="">
                    </div>

                    <div class="regdiv clearfix">
                        <div>
                            {{Form::open(['url' => 'contact-us'])}}
                                <div class="form-group">
                                    <label>Nama<span class="text-success">*</span></label>
                                    {{Form::text('name', null, ['class' => 'form-control'])}}
                                </div>

                                <div class="form-group">
                                    <label>Email<span class="text-success">*</span></label>
                                    {{Form::text('name', null, ['class' => 'form-control'])}}
                                </div>

                                <div class="form-group">
                                    <label>Telp<span class="text-success">*</span></label>
                                    {{Form::text('telp', null, ['class' => 'form-control'])}}
                                </div>

                                <div class="form-group">
                                    <label>Alamat<span class="text-success">*</span></label>
                                    {{Form::text('address', null, ['class' => 'form-control'])}}
                                </div>

                                <div class="form-group">
                                    <label>Pesan<span class="text-success">*</span></label>
                                    {{Form::textarea('message', null, ['class' => 'form-control', 'rows' => 4])}}
                                </div>
                                <button type="submit" class="btn btn-md btn-primary">Submit</button>
                            {{Form::close()}}
                        </div>
                        <div>
                            <div class="text text-lg">
                                <div class="contact-text">
                                    <div>
                                        <div class="labels">Working Hours</div>
                                        <div class="value">
                                            Mon - Fri : 9 am - 6 pm<br>
                                            Sat - Sun : Off
                                        </div>
                                    </div>

                                    <div>
                                        <div class="labels">Head Office</div>
                                        <div class="value">
                                            <span>Mealtime.id</span><br>
                                            Jl. Pembangunan IV No. 119<br>
                                            Petojo Utara, Gambir,<br>
                                            Jakarta Pusat 11030
                                        </div>
                                    </div>

                                    <div>
                                        <div class="labels">Contact</div>
                                        <div class="value">
                                            0857 2085 8023<br>
                                            <a href="#">customer@mealtime.id</a>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="labels">Sales</div>
                                        <div class="value">
                                            <a href="#">sales@mealtime.id</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix" style="display: none">
                        <div class="office-details">

                            <h4 class="bold-head">head office</h4>
                            <div>Mealtime.id</div>
                            <div>Jl. Pembangunan IV No. 119</div>

                            <h4 class="bold-head">branch office</h4>
                            <h4>aj - 325,</h4>
                            <h4>sec - 2, saltlake</h4>
                            <h4>kolkata - 700091</h4>

                        </div>

                        <div class="write-us">
                            <h3>write to us</h3>
                            <form id="contactForm">
                                <div class="main-holder clearfix">
                                    <div class="placehold">Name</div>
                                    <div class="input-holder">
                                        <input type="text" id="contactUsName" name="contactUsName">
                                    </div>
                                </div>

                                <div class="main-holder clearfix">
                                    <div class="placehold">Email</div>
                                    <div class="input-holder">
                                        <input type="text" id="contactUsEmail" name="contactUsEmail">
                                    </div>
                                </div>

                                <div class="main-holder clearfix">
                                    <div class="placehold">Message</div>
                                    <div class="input-holder">
                                        <textarea rows="6" id="contactUsMsg" name="contactUsMsg"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <button class="button">submit now</button>
                                    <div class="contact-page-form form-message" >
                                        <div><div class="loader">Loading...</div></div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                    <div class="socials-holder" style="margin-top: 60px;">
                        <h4>stay connected with us</h4>
                        <div class="social-connect clearfix">
                            <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="gplus"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </article>
            </div> <!-- CONTAINER ends -->
        </section> <!-- MAIN CONTENT ends -->
    </div>
@stop