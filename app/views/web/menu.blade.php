@extends('layouts.default')

@section('content')
    <div class="menu-page">
        <section class="banner">
            <div class="good-food">
                <div class="container">
                    <article class="banner-caption">
                        <h2>IT'S ALL ABOUT GOOD FOOD</h2>
                    </article>
                </div>
            </div>
        </section>
        <section class="main-content">
            <div class="container">
                <form class="food-options-form">
                    <div class="caption">
                        <h3>select your food</h3>
                        <h6>A special way to select your food from our wide rnage of recipe.</h6>
                    </div>
                    <div class="options-button clearfix">
                        <script type="text/javascript">var selectedDishes = 0;</script>
                        <div class="form-btn-area clearfix">
                            <div class="no-of-dishes">
                                <h6>
                                    You have selected
                        <span class="selected-dishes-no">
                          <script type="text/javascript">document.write(selectedDishes);</script>
                        </span>
                                    dishes.
                                </h6>
                                <div>
                                    <img src="assets/public/images/chef-hat-icon-grey.png" height="40" width="41" alt="">
                        <span class="selected-dishes-no">
                          <script type="text/javascript">document.write(selectedDishes);</script>
                        </span>
                                </div>
                            </div>
                            <div class="button-holder">
                                <a class="button red-btn place-order-now-btn" href="javascript:void(0)">order now</a>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- ============= SEARCH-MENU-LIST ================== -->
                <article class="search-menu-list brkfast show">
                    <div class="head">
                        <img src="assets/public/images/food-menu-icon.png" alt="">
                        <h2>breakfast</h2>
                        <h6>Your search results listed below</h6>
                    </div>
                    <div class="menu-items-wrapper" >
                        <ul class="brkfstSlider clearfix">
                            <!-- ============= SEARCH-MENU-LIST =============== -->
                            <li data-name='Red Velvet Cupcakes' data-price='12.49' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Red Velvet Cupcakes</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$12.49</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Vanilla Crème Brûlée' data-price='10.90' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item2.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Vanilla Crème Brûlée</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$10.90</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Glazed Carrot Cake' data-price='13.29' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item3.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Glazed Carrot Cake</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$13.29</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Praline-Iced Brownies' data-price='23.25' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item4.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Praline-Iced Brownies</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$23.25</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Whoopie Pies' data-price='64.13' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item5.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Whoopie Pies</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$64.13</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Spaghetti al Forno' data-price='11.45' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item6.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Spaghetti al Forno</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$11.45</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Pumpkin Lasagne' data-price='17.89' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item7.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Pumpkin Lasagne</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$17.89</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li data-name='Baked Ravioli' data-price='53.21' class="own">
                                <div class="search-menu-items clearfix clearfix">
                                    <figure>
                                        <img src="assets/public/images/search-food-item8.jpg" alt="">
                                    </figure>
                                    <div class="figcaption clearfix">
                                        <div>
                                            <h3>Baked Ravioli</h3>
                                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                            <div>
                                                <h6>Ingredients</h6>
                                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                                <h6>Customer Review</h6>
                                                <div class="cust-rating" >
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="red fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-add-select clearfix">
                                            <a class="button white-btn clicked" href="javascript:void(0)">
                                                <span class="desk">add to list</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button green-btn" href="javascript:void(0)">
                                                <span class="desk">selected</span>
                                                <span class="mob"><i class="fa fa-check"></i></span>
                                            </a>
                                            <a class="button red-btn" href="javascript:void(0)">X</a>
                                            <h2>$53.21</h2>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!-- ============================================== -->
                        </ul>
                        <div class="nav-btns">
                            <a class="left-btn" href="javascript:void(0)"><i class="fa fa-angle-left"></i></a>
                            <a class="right-btn" href="javascript:void(0)"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </article>
                <!-- ================================================= -->
                <!-- </div> -->

                <div class="complete-search clearfix">
                    <div class="left-sectn">
                        <h3>completed your search, yipee!</h3>
                        <h6>Now one more step to order your special treat</h6>
                    </div>
                    <div class="right-sectn clearfix">
                        <div class="no-of-dishes">
                            <h6>
                                You have selected
                      <span class="selected-dishes-no">
                        <script type="text/javascript">document.write(selectedDishes);</script>
                      </span>
                                dishes.
                            </h6>
                            <div>
                                <img src="assets/public/images/chef-hat-icon-grey.png" height="40" width="41" alt="">
                      <span class="selected-dishes-no">
                        <script type="text/javascript">document.write(selectedDishes);</script>
                      </span>
                            </div>
                        </div>
                        <!-- NO-OF-DISHES ends -->
                        <div class="button-holder">
                            <a class="button red-btn place-order-now-btn" href="javascript:void(0)">order now</a>
                        </div>
                        <!-- BUTTON-HOLDER ends -->
                    </div>
                    <!-- RIGHT-SECTN ends -->
                </div>
            </div>
            <!-- CONTAINER ends -->
        </section>
    </div>

    @include('block.modal.order')
@stop