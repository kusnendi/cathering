@extends('layouts.default')

@section('content')
	<div class="food-gallery">
		<section class="banner">
			<!-- <article class="banner-img">
				<img src="assets/public/images/food-solution-bg.jpg" alt="">
			</article> -->

			<div class="container">
				<article class="banner-caption">
					<h5>Experience our special food</h5>
					<h2>food gallery</h2>
				</article>
			</div>
		</section>

		<section class="main-content">
			<div class="container">
				
				<ul class="food-type-list clearfix" id="food-type-list">
					<li><a href="#food-type-list" class="selected" data-filter="*">All</a></li>
					<li><a href="#food-type-list" data-filter=".appetizers">appetizers</a></li>
					<li><a href="#food-type-list" data-filter=".desserts">desserts</a></li>
					<li><a href="#food-type-list" data-filter=".main_dishes">main dishes</a></li>
					<li><a href="#food-type-list" data-filter=".salads">salads</a></li>
					<li><a href="#food-type-list" data-filter=".drinks">drinks</a></li>
				</ul>

				<article class="content-wrapper clearfix food-items" id="food-items">
					<div class="food-item-wrapper appetizers">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/appetizers/appetizers_01.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Apple Crumb Squares</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span>609.99</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>

					<div class="food-item-wrapper main_dishes">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/appetizers/appetizers_02.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Red Velvet Cupcakes</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="red"></span> non-vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper appetizers">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="assets/public/images/food-gallery/appetizers/appetizers_03.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Vanilla Crème Brûlée</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					
					<div class="food-item-wrapper main_dishes">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="assets/public/images/food-gallery/appetizers/appetizers_04.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Pear-Cranberry Pie</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="red"></span> non-vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper appetizers">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="assets/public/images/food-gallery/appetizers/appetizers_05.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Glazed Carrot Cake</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="assets/public/images/food-gallery/desserts/desserts_01.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Praline-Iced Brownies</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="assets/public/images/food-gallery/desserts/desserts_02.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Lemon Cheesecake</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/desserts/desserts_0')}}3.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Nutty Popcorn Fudge</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/desserts/desserts_0')}}4.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Whoopie Pies</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/desserts/desserts_0')}}5.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Vanilla Buttercream</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper desserts">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/desserts/desserts_0')}}6.jpg" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Elf Cookies</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>

					<div class="food-item-wrapper drinks">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/drinks/drink_01.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Snowman Cupcakes</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper drinks">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/drinks/drink_02.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Chocolate Mice</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper drinks">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/drinks/drink_02.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Chocolate Mice</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper drinks">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/drinks/drink_03.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Coconut Squares</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper drinks">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/drinks/drink_04.jpg')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Royal Icing</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper salads">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/salads/salade_01.jp')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Present Cookies</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper salads">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/salads/salade_02.jp')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Single Piecrust</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper salads">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/salads/salade_03.jp')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Coconut Cream Pie</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
					<div class="food-item-wrapper salads">
						<div class="food-item">
							<div class="figure imgLiquidFill imgLiquid">
								<img src="{{asset('assets/public/images/food-gallery/salads/salade_04.jp')}}" alt="">
							</div>

							<div class="figcaption clearfix">
								<h3>Stack Pie</h3>
								<div class="food-name-type">
									<!-- <h3>Food name</h3> -->
									<div class="clearfix">
										<h4><span class="green"></span> vegetarian</h4>
										<h4><span class="red"></span> 45% hot</h4>
									</div>
								</div>
								<h2><span>$</span> 09</h2>
							</div>

							<div class="heart-ribbon">
								<i class="fa fa-heart"></i>
								<!-- <div class="ribbon-after"></div> -->
								<h5>235</h5>
							</div>
						</div>
					</div>
				</article>
				
			</div> <!-- CONTAINER ends -->
		</section> <!-- MAIN CONTENT ends -->
	</div>
@stop()