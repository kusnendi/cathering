@extends('layouts.default')

@section('content')
    <div class="homepage">

        @include('block.carousel.carousel')

        <div class="featured-food">
            <div class="container">
                <div class="featured-menu-desc">
                    <h1 class="title">Makan Siang Anda untuk Kamis, 07 April 2016</h1>
                    <h4 class="sub">Silahkan <a href="#" class="text-danger">Login</a> untuk memesan.</h4>
                    <h3 class="title-featured">Chef Recommendation Menu</h3>
                </div>
                <div class="card-featured-food" style="display: none;">
                    @include('web.index.components.featured-food')
                </div>
                <div class="row">
                    <div class="col-md-12">
                    @include('block.carousel.swiper')
                    </div>
                </div>
                <div class="" style="padding-top: 15px">
                    <button class="btn btn-danger btn-block">Take it All</button>
                </div>
            </div>
        </div>

        @include('web.index.components.food-category')

        @include('web.index.components.delicious-menu')

        @include('block.testimoni')

        @include('common.newsletter')

    </div> <!-- HOMEPAGE ends -->

    @include('block.modal.login')
@stop