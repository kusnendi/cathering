@extends('layouts.default')

@section('content')
    {{--*/ $ppn = 0 /*--}}
    {{--*/ $payment_code = rand(-100,100) /*--}}
    <div class="checkout-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="text-center pt30">
                    <h2 class="text-primary">Checkout Process</h2>
                </div>

                <div class="cart-wrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default no-round">
                                <div class="panel-body">
                                    <div class="cart-checkout-item">
                                        <div class="head-step">
                                            Step 1. Login/Register
                                        </div>
                                        <div class="cart-checkout-step cart-checkout-step-1" style="display: {{(Auth::check()) ? 'none' : 'block'}};">
                                            <div class="cart-checkout-login">
                                                <div class="div">
                                                    <div>
                                                        <div class="title">Returning Customer</div>
                                                        {{Form::open(['url' => 'login'])}}
                                                        <div class="form-group">
                                                            <label>Email<span class="text-success">*</span></label>
                                                            {{Form::email('email', null, ['class' => 'form-control input-sm', 'autofocus' => 'autofocus'])}}
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Password<span class="text-success">*</span></label>
                                                            {{Form::password('password', ['class' => 'form-control input-sm'])}}
                                                        </div>
                                                        <div class="form-group checkbox">
                                                            <label for="remember">
                                                                Remember Me
                                                            </label>
                                                            <input type="checkbox" id="remember" name="remember_token" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary" style="padding-left: 30px; padding-right: 30px">Login</button>
                                                            <button type="button" class="btn btn-link">Forgot Password?</button>
                                                        </div>
                                                        {{Form::close()}}
                                                    </div>
                                                    <div>
                                                        <div class="title">I Dont Have An Account</div>
                                                        <button class="btn btn-primary btn-block" type="button">Register Now</button>
                                                        <div class="or">OR</div>
                                                        <button class="btn btn-primary btn-block" type="button">Checkout As Guest</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!--/step1-->

                                    <div class="cart-checkout-item">
                                        <div class="head-step">Step 2. Cek Pesanan Anda</div>
                                        <div class="cart-checkout-step cart-checkout-step-2" style="display: {{(Auth::check()) ? 'block' : 'none'}};">
                                            @foreach($carts as $cart)
                                                {{--*/ $extra = unserialize($cart->extra) /*--}}
                                                <div class="order-item">
                                                    <div class="img">
                                                        <img src="{{asset('assets/public/products/1.jpg')}}" class="img-responsive" />
                                                    </div>
                                                    <div class="desc">
                                                        <div>
                                                            <div class="title" style="width: 250px">{{$extra['product']}}<br/><small>Rp {{$extra['display_price']}}</small></div>
                                                            <div class="quanity-wrap">
                                                                <div class="quantity">
                                                                    <input type="number" value="{{$cart->amount}}" min="1" class="" name="qty[]" autofocus="autofocus" style="width: 70px; padding: 10px; font-size: 16px; font-weight: bold; text-align: center"/>
                                                                </div>
                                                                <div class="remove" onclick="location='/login'"></div>
                                                            </div>
                                                            <div class="price">Rp {{number_format($cart->amount*$cart->price,0,',','.')}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="cart-checkout-btn">
                                                <button type="button" class="btn btn-primary btn-lg btn-cart-step" data-step="3">Lanjutkan</button>
                                            </div>
                                        </div>
                                    </div> <!--/step2-->

                                    {{Form::open(['url' => 'checkout'])}}
                                    @if($checkout_flag == 'points')
                                        {{Form::hidden('shipping_cost', 0)}}
                                    @endif
                                    @if($checkout_flag != 'points')
                                    <div class="cart-checkout-item">
                                        <div class="head-step">Step 3. Waktu & Tanggal Pengiriman</div>
                                        <div class="cart-checkout-step cart-checkout-step-3">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label for="deliverydate">Tanggal Pengiriman</label>
                                                        <input type="date" id="deliverydate" name="ship_date" class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="deliverytime">Waktu Pengiriman</label>
                                                        <input type="time" id="deliverytime" name="ship_time" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cart-checkout-btn">
                                                <button type="button" class="btn btn-primary btn-lg btn-cart-step" data-step="4">Lanjutkan</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cart-checkout-item">
                                        <div class="head-step">Step 4. Alamat Pengiriman</div>
                                        <div class="cart-checkout-step cart-checkout-step-4">
                                            <div style="margin-left: 20px">
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>Telp/Handphone</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">62</span>
                                                            <input type="text" class="form-control" name="phone" pattern="[0-9]{10,11}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group radio">
                                                <label class="label-lg">
                                                    Alamat Baru
                                                    <input type="radio" name="s_address" value="a_customer" />
                                                </label>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Area</label>
                                                            {{Form::select('location', ['Jakarta Pusat', 'Jakarta Barat'], null, ['class' => 'form-control', 'title' => 'Pilih Area'])}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group radio">
                                                <label class="label-lg">
                                                    Ambil di dapur Mealtime.id
                                                    <input type="radio" name="s_address" value="a_mealtime" />
                                                </label>
                                                <div>
                                                    <small>Silahkan Menghubungi Customer Service Mealtime di 021 2933 0000</small>
                                                </div>
                                            </div>
                                            <div class="cart-checkout-btn">
                                                <button type="button" class="btn btn-link btn-lg btn-cart-step" data-step="3">Sebelumnya</button>
                                                <button type="button" class="btn btn-primary btn-lg btn-cart-step" data-step="5">Lanjutkan</button>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="cart-checkout-item" id="checkoutStep5">
                                        <div class="head-step">Step {{($checkout_flag == 'points') ? 3 : 5}}. Metode Pembayaran</div>
                                        <div class="cart-checkout-step cart-checkout-step-{{($checkout_flag == 'points') ? 3 : 5}}">
                                            @if($total == 0)
                                                <div class="alert alert-warning">
                                                    <p>No payment required.</p>
                                                </div>
                                            @else
                                                @if($checkout_flag != 'points')
                                                    <div class="form-group radio">
                                                        <label class="label-lg">
                                                            COD
                                                            <input type="radio" name="payment_method" value="1" />
                                                        </label>
                                                        <div class="payment-info">
                                                            <small>Bayar cash ketika pengantaran</small>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="form-group radio">
                                                    <label class="label-lg">
                                                        Transfer
                                                        <input type="radio" name="payment_method" value="2" checked />
                                                    </label>
                                                    <div>
                                                        <div class="payment-info">
                                                            <span style="color: black">5000 123 888</span> PT. ABADI CIPTA PRIMAFIT<br/>
                                                            (BCA KCP Harmoni)<br/>
                                                            atau<br/>
                                                            <span style="color: #000000">115 00 0123678</span> PT. ABADI CIPTA PRIMAFIT<br/>
                                                            (Bank Mandiri KCP Harmoni)<br/>
                                                            <small>*Pesanan hanya akan diproses jika pembayaran telah dikonfirmasi.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($checkout_flag != 'points')
                                                    <div class="form-group radio">
                                                        <label class="label-lg">
                                                            Point (Anda memiliki 0 Point)
                                                            <input type="radio" name="payment_method" value="3" />
                                                        </label>
                                                        <div class="payment-info">
                                                            <small>Bayar menggunakan point credit.</small>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="form-group">
                                                <label for="notes">Catatan</label>
                                                <textarea id="notes" name="notes" class="form-control" rows="3"></textarea>
                                            </div>
                                            <div class="cart-checkout-btn">
                                                <button type="button" class="btn btn-link btn-lg btn-cart-step" data-step="{{($checkout_flag == 'points') ? 2 : 4}}">Sebelumnya</button>
                                                <button type="submit" class="btn btn-primary btn-lg">Selesaikan Belanja</button>
                                            </div>
                                        </div>
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="cart-confirmation-col pinned">
                                <div class="panel panel-default no-round">
                                    <div class="panel-body">
                                        <div class="panel-head text-center">Informasi Pesanan</div>
                                        <div class="cart-confirmation-info">
                                            @if($checkout_flag != 'points')
                                            <div class="item">
                                                <div class="labels">Tangal Pengiriman</div>
                                                <div class="value">16 Maret 2016</div>
                                            </div>
                                            <div class="item">
                                                <div class="labels">Waktu Pengiriman</div>
                                                <div class="value">21:00</div>
                                            </div>
                                            @endif
                                            <div class="item" style="margin-bottom: 35px">
                                                <div class="labels">Metode Pembayaran</div>
                                                <div class="value">Transfer</div>
                                            </div>
                                            <div class="item">
                                                <div class="labels">Sub Total</div>
                                                <div class="value">Rp {{number_format($subtotal->subtotal,0,',','.')}}</div>
                                            </div>
                                            <div class="item" style="display: none">
                                                <div class="labels">PPN</div>
                                                <div class="value">+ Rp {{number_format($ppn,0,',','.')}}</div>
                                            </div>
                                            @if($checkout_flag != 'points')
                                            <div class="item">
                                                <div class="labels">Ongkos Kirim</div>
                                                <div class="value">Free</div>
                                            </div>
                                            @endif
                                            <div class="item">
                                                <div class="labels">Kode Pembayaran</div>
                                                <div class="value">{{$payment_code}}</div>
                                            </div>
                                            @if($voucher)
                                                <div class="item">
                                                    <div class="labels text-info">{{$voucher['code']}}</div>
                                                    <div class="value">- Rp {{number_format($voucher['debit'],0,',','.')}}</div>
                                                </div>
                                            @else
                                            <div class="gift-cert" style="padding-bottom: 15px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="voucher" placeholder="Gift Cert atau Voucher" />
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-danger apply-voucher" title="Apply"><i class="fa fa-chevron-right"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="cart-confirmation-info cart-confirmation-info-total">
                                            <div class="item">
                                                <div class="labels">Total</div>
                                                <div class="value">Rp {{number_format($total,0,',','.')}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!--/row-->
                </div>

            </div>
        </section>
    </div>
@stop