@extends('layouts.default')

@section('content')
    <div class="point-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="text-center pt30">
                    <h2 class="text-center">Buy Meal Point</h2>
                </div>

                <div class="buy-point clearfix">
                    <div class="buy-point-item item-2">
                        <div class="note">Trial</div>
                        <div class="buy-point-lunch">3 Lunch Box</div>
                        <div class="buy-point-points">
                            <div>15</div>
                            <div class="lbl">Points</div>
                        </div>
                        <div class="buy-point-price price-string">Rp 85.000,00</div>
                        <button type="button" class="btn btn-block btn-primary add-cart" data-skuld="1" data-price="85000" data-product="15 Points" data-amount="1">Tambah Point</button>
                    </div>
                    <div class="buy-point-item item-2">
                        <div class="buy-point-lunch">10 Lunch Box</div>
                        <div class="buy-point-points">
                            <div>50</div>
                            <div class="lbl">Points</div>
                        </div>
                        <div class="buy-point-price price-string">Rp 260.000,00</div>
                        <button type="button" class="btn btn-block btn-primary add-cart" data-skuld="2" data-price="260000" data-product="50 Points" data-amount="1">Tambah Point</button>
                    </div>
                    <div class="buy-point-item item-2">
                        <div class="buy-point-lunch">20 Lunch Box</div>
                        <div class="buy-point-points">
                            <div>100</div>
                            <div class="lbl">Points</div>
                        </div>
                        <div class="buy-point-price price-string">Rp 500.000,00</div>
                        <button type="button" class="btn btn-block btn-primary add-cart" data-skuld="3" data-price="500000" data-product="100 Points" data-amount="1">Tambah Point</button>
                    </div>
                    <div class="buy-point-item item-2">
                        <div class="buy-point-lunch">60 Lunch Box</div>
                        <div class="buy-point-points">
                            <div>300</div>
                            <div class="lbl">Points</div>
                        </div>
                        <div class="buy-point-price price-string">Rp 1.400.000,00</div>
                        <button type="button" class="btn btn-block btn-primary add-cart" data-skuld="4" data-price="1400000" data-product="300 Points" data-amount="1">Tambah Point</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop