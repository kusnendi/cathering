@extends('layouts.default')

@section('content')
    <div class="account-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="page-content">
                    <div class="acc-div">
                        @include('block.account.navigation')

                        <div class="acc-cont">
                            <div class="row">
                                <div class="col-sm-push-8 col-lg-offset-1 col-sm-3">
                                    @include('block.account.mypoints')
                                </div>

                                <div class="col-sm-pull-4 col-sm-8">
                                    <h2>Rating</h2>
                                    <div class="sub-heading">Beri kami feedback untuk pelayanan yang lebih baik.</div>

                                    {{Form::open(['url' => 'accounts/change-password'])}}

                                    <div class="form-group">
                                        <label for="fullname" class="control-label">Password Sekarang</label>
                                        {{Form::password('password', ['class' => 'form-control input-lg'])}}
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-sm-6">
                                            <label for="phone" class="control-label">Password Baru</label>
                                            {{Form::password('new_password', ['class' => 'form-control input-lg'])}}
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="phone" class="control-label">Ulangi Password Baru</label>
                                            {{Form::password('confirm_password', ['class' => 'form-control input-lg'])}}
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-danger btn-lg">Simpan</button>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop