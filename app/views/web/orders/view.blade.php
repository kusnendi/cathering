@extends('layouts.default')

@section('content')
    <div class="account-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="page-content">
                    <div class="acc-div">
                        @include('block.account.navigation')

                        <div class="acc-cont">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div style="display: table; width: 100%; line-height: 24px">
                                        <div style="display: table-cell; width: 50%">
                                            <h2>Order #{{$order->id}} / <span style="font-size: 14px">{{$order->created_at}}</span></h2>
                                        </div>
                                        <div style="display: table-cell; width: 50%">
                                            <div class="pull-right">Status: <label class="label label-info">{{$order->status}}</label></div>
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th width="50%">Product</th>
                                                <th width="15%" class="text-left">Price</th>
                                                <th width="15%" class="text-center">Quantity</th>
                                                <th width="20%" class="text-right">Subtotal</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order->details as $item)
                                                {{--*/ $extra = unserialize($item->extra) /*--}}
                                                <tr>
                                                    <td>{{$extra['product']}}</td>
                                                    <td class="text-left">Rp {{$extra['display_price']}}</td>
                                                    <td class="text-center">{{$item->amount}}</td>
                                                    <td class="text-right">Rp {{number_format($item->price*$item->amount,0,',','.')}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr/>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-5 col-xs-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-body no-round">
                                                        <div class="panel-head">Summary</div>
                                                        <div class="order-summary">
                                                            <div class="item">
                                                                <div class="labels">Metode Pembayaran</div>
                                                                <div class="value">{{($order->payment_id) ? 'Transfer' : 'Voucher'}}</div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="labels">Subtotal</div>
                                                                <div class="value">Rp {{number_format($order->subtotal,0,',','.')}}</div>
                                                            </div>
                                                            @if($order->coupons)
                                                                <div class="item">
                                                                    <div class="labels text-info">{{$order->coupons->coupon->code}}</div>
                                                                    <div class="value">- Rp {{number_format($order->coupons->amount-$order->coupons->debit,0,',','.')}}</div>
                                                                </div>
                                                            @endif
                                                            <div class="order-summary order-summary-total">
                                                                <div class="item">
                                                                    <div class="labels">Total</div>
                                                                    <div class="value">Rp {{number_format($order->total,0,',','.')}}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop