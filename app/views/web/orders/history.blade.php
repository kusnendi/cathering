@extends('layouts.default')

@section('content')
    <div class="account-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="page-content">
                    <div class="acc-div">
                        @include('block.account.navigation')

                        <div class="acc-cont">
                            <div class="row">
                                <div class="col-sm-push-8 col-lg-offset-1 col-sm-3">
                                    @include('block.account.mypoints')
                                </div>

                                <div class="col-sm-pull-4 col-sm-8">
                                    <h2>Orders</h2>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th width="10%">Order ID</th>
                                                <th width="20%">Tanggal</th>
                                                <th width="10%">Status</th>
                                                <th width="10%" class="text-center">Pembayaran</th>
                                                <th width="5%"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($orders->count())
                                                @foreach($orders as $order)
                                                    <tr>
                                                        <td>{{$order->id}}</td>
                                                        <td>{{$order->created_at}}</td>
                                                        <td>{{$order->status}}</td>
                                                        <td class="text-center">
                                                            @if($order->status == 'O')
                                                            <a href="{{url('orders/payment-confirmation?order_id='.$order->id)}}" class="btn btn-xs btn-danger" title="Konfirmasi Pembayaran Anda">Konfirmasi</a>
                                                            @else
                                                            <button class="btn btn-default btn-xs" disabled>Konfirmasi</button>
                                                            @endif
                                                        </td>
                                                        <td class="text-right">
                                                            <a href="{{url('orders',['view',$order->id])}}" class="btn btn-info btn-xs" title="Lihat Detail Order"><i class="fa fa-list"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="5" style="font-size: 13px">Oops, tidak ada transaksi yang ditemukan.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop