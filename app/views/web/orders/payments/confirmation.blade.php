@extends('layouts.default')

@section('content')
    <div class="account-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="page-content">
                    <div class="acc-div">
                        @include('block.account.navigation')

                        <div class="acc-cont">
                            <div class="row">
                                <div class="col-sm-push-8 col-lg-offset-1 col-sm-3">
                                    @include('block.account.mypoints')
                                </div>

                                <div class="col-sm-pull-4 col-sm-8">
                                    <h2>Payment Confirmation</h2>
                                    <div class="sub-heading">Terimakasih atas pembayaran yang telah dilakukan. Mohon mengisi data ini untuk proses pemeriksaan pembayaran dan pengiriman pesanan Anda. Tanpa konfirmasi ini, kami tidak dapat mengetahui pembayaran Anda.</div>

                                    @if(count($orders))
                                        {{Form::open(['url' => 'orders/payment-confirmation'])}}

                                        <div class="form-group">
                                            <label for="orderID" class="control-label">Order ID</label>
                                            {{Form::select('order_id', $orders, (Input::has('order_id') ? Input::get('order_id') : null), ['class' => 'form-control'])}}
                                        </div>

                                        <div class="form-group">
                                            <label for="senderName" class="control-label">Nama</label>
                                            {{Form::text('sender_name', null, ['class' => 'form-control'])}}
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-4">
                                                <label for="dateTransfer" class="control-label">Tanggal Transfer</label>
                                                {{Form::input('text','date_transfer', null, ['class' => 'form-control date'])}}
                                            </div>
                                            <div class="col-md-4">
                                                <label for="amount" class="control-label">Nilai Transfer</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    {{Form::text('amount', null, ['class' => 'form-control'])}}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="bank" class="control-label">Bank Tujuan</label>
                                                {{Form::select('to', ['bca' => 'BCA','mandiri' => 'MANDIRI'],'bca', ['class' => 'form-control'])}}
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-danger">Submit</button>
                                        {{Form::close()}}
                                    @else
                                        <div class="alert alert-info">
                                            <p>Belum ada order, silakan pesan makanan anda.</p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop