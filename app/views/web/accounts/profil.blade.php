@extends('layouts.default')

@section('content')
    <div class="account-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="page-content">
                    <div class="acc-div">
                        @include('block.account.navigation')

                        <div class="acc-cont">
                            <div class="row">
                                <div class="col-sm-push-8 col-lg-offset-1 col-sm-3">
                                    @include('block.account.mypoints')
                                </div>

                                <div class="col-sm-pull-4 col-sm-8">
                                    <h2>Profile</h2>

                                    {{Form::open(['url' => 'accounts'])}}
                                        <div class="form-group">
                                            <label for="email" class="control-label">Email</label>
                                            {{Form::email('email', Auth::user()->email, ['class' => 'form-control input-lg', 'disabled' => 'disabled'])}}
                                        </div>

                                        <div class="form-group">
                                            <label for="fullname" class="control-label">Nama</label>
                                            {{Form::text('fullname', $profil->fullname, ['class' => 'form-control input-lg'])}}
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-sm-6">
                                                <label for="location" class="control-label">Area</label>
                                                {{Form::select('location', ['Jakarta Pusat', 'Jakarta Barat'], null, ['class' => 'form-control input-lg', 'title' => 'Pilih Area'])}}
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="phone" class="control-label">Telepon</label>
                                                {{Form::text('phone', ($profil->phone) ? $profil->phone : null, ['class' => 'form-control input-lg'])}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="address" class="control-label">Alamat</label>
                                            {{Form::textarea('address', ($profil->address ? $profil->address : null), ['class' => 'form-control input-lg', 'rows' => 3])}}
                                        </div>

                                        <div class="form-group checkbox">
                                            <label for="reminder">Tolong ingatkan saya untuk memesan sebelum jam 3 sore melalui email</label>
                                            <input type="checkbox" name="reminder" id="reminder" value="0" />
                                        </div>

                                        <button type="submit" class="btn btn-danger btn-lg">Simpan</button>
                                    {{Form::close()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop