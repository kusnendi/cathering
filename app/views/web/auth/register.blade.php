@extends('layouts.default')

@section('content')
    <div class="register-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="content-wrapper" style="border: transparent;">
                    <div class="text-center">
                        <h2 class="text-primary">Register</h2>
                    </div>

                    <div class="regdiv clearfix" style="padding-top: 30px">
                        <div>
                            <div class="header text-center">Buat Account Baru</div>
                            {{Form::open(['url' => 'register'])}}
                            <div class="form-group">
                                <label class="control-label">Nama<span class="text-success">*</span></label>
                                {{Form::text('name', null, ['class' => 'form-control input-lg'])}}
                                @if($errors->has('name'))
                                <div class="text-danger"><small>{{$errors->first('name')}}</small></div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Email<span class="text-success">*</span></label>
                                {{Form::email('email', null, ['class' => 'form-control input-lg'])}}
                            </div>

                            <div class="form-group">
                                <label>Password<span class="text-success">*</span></label>
                                {{Form::password('password', ['class' => 'form-control input-lg'])}}
                                @if($errors->has('name'))
                                <div class="text-danger"><small>{{$errors->first('password')}}</small></div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Ulangi Password<span class="text-success">*</span></label>
                                {{Form::password('confirm_password', ['class' => 'form-control input-lg'])}}
                            </div>
                            <button type="submit" class="btn btn-lg btn-block btn-primary">Submit</button>
                            {{Form::close()}}
                        </div>
                        <div>
                            <div class="text text-lg">
                                <div class="contact-text text-center">
                                    <div class="header" style="margin-bottom: 0px">Already a Member ?</div>
                                    <a href="{{URL::to('login')}}" class="btn btn-block btn-lg btn-primary">Login with Email</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop