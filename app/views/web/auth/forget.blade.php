@extends('layouts.default')

@section('content')
    <div class="login-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="content-wrapper" style="border: transparent">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <h2 class="text-primary text-center">Lupa Password</h2>
                            <div class="sub-heading text-center" style="padding-bottom: 30px">Silahkan masukkan email anda untuk Recovery Password.</div>
                            {{Form::open(['url' => 'password/remind'])}}
                                <div class="form-group">
                                    <label>Email<span class="text-success">*</span></label>
                                    {{Form::email('email', null, ['class' => 'form-control input-lg', 'required' => 'required'])}}
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-primary btn-lg" type="submit" style="padding-left: 30px; padding-right: 30px">Reset Password</button>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop