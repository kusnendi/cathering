@extends('layouts.default')

@section('content')
    <div class="login-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="content-wrapper" style="border: transparent">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <h2 class="text-primary text-center">Reset Password</h2>
                            <div class="sub-heading text-center" style="padding-bottom: 30px">Perbarui password anda sekarang.</div>
                            <form action="{{ action('RemindersController@postReset') }}" method="POST">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label>Email<span class="text-success">*</span></label>
                                    {{Form::email('email', $email->email, ['class' => 'form-control input-lg', 'readonly' => 'readonly'])}}
                                </div>

                                <div class="form-group">
                                    <label>Password<span class="text-success">*</span></label>
                                    {{Form::password('password', ['class' => 'form-control input-lg'])}}
                                </div>

                                <div class="form-group">
                                    <label>Ulangi Password<span class="text-success">*</span></label>
                                    {{Form::password('password_confirmation', ['class' => 'form-control input-lg'])}}
                                </div>
                                <button type="submit" class="btn btn-lg btn-block btn-primary">Reset Password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop