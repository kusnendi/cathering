@extends('layouts.default')

@section('content')
    <div class="login-page">
        <section class="main-content" style="background-color: #ffffff">
            <div class="container">
                <div class="content-wrapper" style="border: transparent">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4">
                            <h2 class="text-primary text-center">Login</h2>
                            {{Form::open(['url' => 'login'])}}
                                <div class="form-group">
                                    <label>Email<span class="text-success">*</span></label>
                                    {{Form::email('email', null, ['class' => 'form-control input-lg'])}}
                                </div>
                                <div class="form-group">
                                    <label>Password<span class="text-success">*</span></label>
                                    {{Form::password('password', ['class' => 'form-control input-lg'])}}
                                </div>
                                <div class="form-group text-center">
                                    <button class="btn btn-primary btn-lg" type="submit" style="padding-left: 30px; padding-right: 30px">Login</button>
                                </div>
                            {{Form::close()}}
                            <div class="text-center">
                                <div class="form-group">
                                    <br>
                                    <a href="{{URL::to('password/remind')}}" class="text-primary">Lupa Password</a>
                                </div>
                                <div class="form-group">
                                    <strong>Belum punya account?</strong>
                                    <a href="{{URL::to('register')}}" class="text-primary">Daftar Gratis</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop