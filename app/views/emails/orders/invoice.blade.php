<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>{{$pageTitle}}</title>
    <style>
        body {
            font-family: 'Roboto Condensed', sans-serif;
            background-color: #f9f9f9;
        }
        .container{
            width: 602px;
            margin: 30px auto;
            padding: 20px 40px;
            background-color: #ffffff;
            border: 1px solid #f2f2f2;
        }
        .item {
            font-size: 12px;
        }
        .item .label {
            text-transform: uppercase;
        }
        .div-table,.item {
            display: table;
            width: 100%;
            padding-bottom: 5px;
        }
        .div-table > div, .item > div {
            display: table-cell;
            width: 50%;
            vertical-align: top;
        }
        .logo {
            font-size: 24px;
            font-weight: bold;
            text-transform: uppercase;
        }
        .strong {
            font-weight: bold;
        }
        .header {
            text-align: right;
            font-size: 24px;
        }
        .text-lead {
            font-size: 20px;
            font-weight: bold;
        }
        .border-bottom {
            border-bottom: 1px solid #f1f1f1;
        }
        .merchant-info,.bill-info {
            font-size: 12px;
        }
        .merchant-info .strong {
            margin-bottom: 5px;
        }
        .merchant-contact .item > div:first-child {
            width: 25%;
        }
        .merchant-contact .item > div:last-child {
            width: 75%;
        }
        .pt30{
            padding-top: 30px;
        }
        .pt20{
            padding-top: 20px;
        }
        .total-invoice {
            font-size: 16px;
            font-weight: bold;
        }
        .table {
            width: 100%;
            font-size: 12px;
            border-collapse: collapse;
            /*border: 1px solid #dedede;*/
        }
        .table th {
            padding: 5px 10px;
            border: 1px solid #dedede;
            background-color: #F5F5F5;
        }
        .table th:first-child {
            text-align: left;
        }
        .table td {
            padding: 5px 10px;
            border: 1px solid #dedede;
        }
        .table td:nth-child(n) {
            text-align: center;
        }
        .table td:first-child {
            text-align: left;
        }
        .no-border {
            border: transparent;
        }
    </style>
</head>
<body onload="{{(Input::get('action') == 'print') ? 'print()' : ''}}">
    <div class="container">
        <div class="div-table border-bottom">
            <div class="logo">Mealtime</div>
            <div class="header">INVOICE</div>
        </div>

        <div class="pt30">
            <div class="div-table">
                <div class="merchant-info">
                    <div class="strong">MEALTIME</div>
                    <div>
                        Jl. Pembangunan IV No. 119 Petojo Utara<br/>
                        Gambir, 10130<br/>
                        Jakarta Pusat
                    </div>
                    <div class="merchant-contact">
                        <div class="item">
                            <div class="label">Phone</div>
                            <div class="value">: 08998206678</div>
                        </div>
                        <div class="item">
                            <div class="label">Website</div>
                            <div class="value">: http://mealtime.id</div>
                        </div>
                        <div class="item">
                            <div class="label">Email</div>
                            <div class="value">: customer@mealtime.id</div>
                        </div>
                    </div>
                </div>
                <div class="invoice-info">
                    <div class="strong">ORDER: #{{sprintf('%06s',$order->id)}}</div>
                    <div class="">
                        <div class="item">
                            <div class="label">Status</div>
                            <div class="value">: {{$order->status}}</div>
                        </div>
                        <div class="item">
                            <div class="label">Tanggal</div>
                            <div class="value">: {{date('Y/m/d, H:i')}}</div>
                        </div>
                        <div class="item">
                            <div class="label">Metode Pembayaran</div>
                            <div class="value">: Transfer</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pt20">
            <div class="div-table">
                <div></div>
                <div class="bill-info">
                    <div class="text-lead">Bill to:</div>
                    <div>
                        {{$order->fullname}}<br/>
                        {{$order->user->profile->address}}<br/>
                        {{$order->user->profile->phone}}<br/>
                    </div>
                </div>
            </div>
        </div>

        <div class="pt30">
            <table class="table">
                <thead>
                <tr>
                    <th width="45%">Item</th>
                    <th width="15%">Quantity</th>
                    <th width="20%">Price</th>
                    <th width="20%">Subtotal</th>
                </tr>
                </thead>

                <tbody>
                @foreach($order->details as $item)
                    {{--*/ $extra = unserialize($item->extra) /*--}}
                    <tr>
                        <td>{{$extra['product']}}</td>
                        <td>{{$item->amount}}</td>
                        <td>Rp {{number_format($item->price,0,',','.')}}</td>
                        <td style="text-align: right">Rp {{number_format($item->price*$item->amount,0,',','.')}}</td>
                    </tr>
                @endforeach
                <tr style="border: transparent">
                    <td colspan="3" style="text-align: right; border: transparent">Subtotal:</td>
                    <td style="text-align: right; border: transparent">Rp {{number_format($order->subtotal,0,',','.')}}</td>
                </tr>
                @if($order->coupons)
                    <?php
                        $code = $order->coupons->coupon->code;
                        $hash = substr($code,0,3);
                        $hash.= preg_replace('([A-L])', '*' ,substr($code,3,4));
                        $hash.= preg_replace('([Q-Z])', '*' ,substr($code,7,5));
                        $hash.= preg_replace('([A-X])', '*' ,substr($code,12,5));
                    ?>
                    <tr style="border: transparent">
                        <td colspan="3" style="font-size:12px; font-weight: bold; text-align: right; border: transparent">{{$hash}}</td>
                        <td style="text-align: right; border: transparent">- Rp {{number_format($order->coupons->amount-$order->coupons->debit,0,',','.')}}</td>
                    </tr>
                @endif
                <tr style="border: transparent">
                    <td colspan="2" style="text-align: right; border: transparent"></td>
                    <td style="text-align: right; border-right: transparent; border-bottom: transparent; border-left: transparent; border-top: 1px solid #dedede"><strong>Total:</strong></td>
                    <td class="total-invoice" style="text-align: right; border-right: transparent; border-bottom: transparent; border-left: transparent; border-top: 1px solid #dedede">Rp {{number_format($order->total,0,',','.')}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>