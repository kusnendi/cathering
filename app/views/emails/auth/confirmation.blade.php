<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Hi {{$fullname}},</h2>

		<div>
			Satu langkah untuk mengaktifkan account anda. Silahkan klik link berikut ini :
			<a href="http://www.mealtime.id/activation?token={{$token}}">http://www.mealtime.id/activation?token={{$token}}</a>

			<p>
			Experience Our Best Food,
			Mealtime
			</p>
		</div>
	</body>
</html>
