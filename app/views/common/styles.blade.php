<link rel="stylesheet" href="{{asset('assets/public/css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/jquery.bxslider.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/animate.css')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/public/css/dist/jquery.datetimepicker.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/owl.theme.css')}}">

<link rel="stylesheet" href="{{asset('assets/public/css/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/desktop.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/tab.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/css/dist/mobile.css')}}">
<link rel="stylesheet" href="{{asset('assets/public/libs/swiper/dist/css/swiper.min.css')}}">
{{HTML::style('assets/public/libs/sweetalert/lib/sweet-alert.css')}}
<link rel='stylesheet' id='css-fancybox-css'  href="{{asset('assets/public/js/lightbox/source/jquery.fancybox.css')}}" type='text/css' media='all' />

<!--[if IE]>
<script src="{{asset('assets/public/js/html5shiv.js')}}"></script>
<link rel="stylesheet" href="{{asset('assets/public/css/dist/ie9.css')}}">
<![endif]-->

<script src="{{asset('assets/public/js/modernizr.custom.50095.js')}}"></script>
<script src="{{asset('assets/public/js/respond.js')}}"></script>
