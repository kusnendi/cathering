<script src="{{asset('assets/public/js/jquery-2.1.1.min.js')}}"></script>
<script src="{{asset('assets/public/libs/swiper/dist/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('assets/public/js/jquery.validate.js')}}"></script>
<script src="{{asset('assets/public/js/jquery.fittext.js')}}"></script>
<script src="{{asset('assets/public/js/jquery.stellar.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/public/js/waypoints.min.js')}}"></script>
<script src="{{asset('assets/public/js/jquery.datetimepicker.js')}}"></script>
<!-- bxSlider Javascript file -->
<script src="{{asset('assets/public/js/jquery.bxslider.min.js')}}"></script>
<script src="{{asset('assets/public/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/public/js/menu-food-carousel.js')}}"></script>
<script src="{{asset('assets/public/js/bootstrap.min.js')}}"></script>
{{HTML::script('assets/public/js/jquery.pin.js')}}
{{HTML::script('assets/public/libs/sweetalert/lib/sweet-alert.js')}}
<script src="{{asset('assets/public/js/imgLiquid-min.js')}}"></script>
<script src="{{asset('assets/public/js/lightbox/source/jquery.fancybox.js')}}"></script>
<script src="{{asset('assets/public/js/form.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/public/js/myCustom.js')}}"></script>
<script src="{{asset('assets/public/js/jquery.isotope.js')}}"></script>

<!-- google maps -->
{{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--}}
<script>
    'use strict';

    $("#order-form").validate({
        rules:{
            orderName:{required: true},
            orderEmail:{required: true,email:true},
            orderAddress:{required: true},
            orderPincode:{required: true},
            orderPhone:{required: true,number:true,minlength:8}
        },
        submitHandler: function (form) {

            var str=''; var i=0;
            $( ".sel" ).each(function( index ) {
                if (i>0)
                { str+="|" }
                str+=$(this).parent('.order-item-price').find('.hid_rcp').val()+':'+$(this).parent('.order-item-price').find('.hid').val()+':'+$(this).val();
                i++;
            });

            var suburl = 'mail.html',
                    orderName=$("#orderName").val(),
                    orderEmail=$("#orderEmail").val(),
                    orderAddress=$("#orderAddress").val(),
                    orderPincode=$("#orderPincode").val(),
                    dishes=str,
                    orderPhone=$("#orderPhone").val(),
                    orderId=$("#orderid").html(),
                    orderTime=$("#order-date").html();

            $('.form-message').show();

            var data={'formid':'order-form','orderName':orderName,'orderEmail':orderEmail,'orderAddress':orderAddress,'orderPincode':orderPincode,'orderPhone':orderPhone,'orderTime':orderTime,'orderId':orderId,'dishes':str};
            $.post(suburl ,  data , function(response){
                $('.form-message').html(response);
                $('.form-message').show();

                $( '#order-form' ).each(function(){
                    this.reset(); //CLEARS THE FORM AFTER SUBMISSION
                });
                //DISABLES FORM SUBMIT BUTTON
                $("#order-form button.green-btn")
                        .prop("disabled",true)
                        .removeClass('green-btn')
                        .addClass('grey-btn disabled');
            });
            return false;
        }
    });
</script>
<script>
    $(document).ready(function(){
        'use strict';

        /*----------------------------------------------------*/
        /*	DATETIME PICKER
         /*----------------------------------------------------*/
        jQuery('#timepicker').datetimepicker({
            datepicker:false,
            format:'H:i'
        });

        jQuery('#datepicker').datetimepicker({
            timepicker:false,
            format:'d.m.Y'
        });

        $('.date').datetimepicker({
            lang: 'id',
            timepicker: false,
            closeOnDateSelect: true,
            minDate: 0,
            dayOfWeekStart: 1,
            format: 'Y/m/d'
        });

        /*-------------------------------------*/
        /*  HOME FEATURED PRODUCT */
        /*-------------------------------------*/
        var swiperView;
        switch(true) {
            case window.innerWidth < 500 :
                swiperView=1;
            break;
            case window.innerWidth > 500 && window.innerWidth <= 850:
                swiperView=2;
            break;
            case window.innerWidth > 850 && window.innerWidth <= 950:
                swiperView=3;
            break;
            default:
                swiperView=4;
            break;
        }

        window.onresize = function () {
            switch(true) {
                case window.innerWidth < 500 :
                    swiperView=1;
                break;
                case window.innerWidth > 500 && window.innerWidth <= 850:
                    swiperView=2;
                break;
                case window.innerWidth > 850 && window.innerWidth <= 950:
                    swiperView=3;
                break;
                default:
                    swiperView=4;
                break;
            }

            var featuredFood = new Swiper('.swiper-container', {
                scrollbar: '.swiper-scrollbar',
                scrollbarHide: true,
                slidesPerView: swiperView,
                spaceBetween: 15,
                grabCursor: true
                //preloadImages: false,
                // Enable lazy loading
                //lazyLoading: true
            });
        };

        var featuredFood = new Swiper('.swiper-container', {
            scrollbar: '.swiper-scrollbar',
            scrollbarHide: true,
            slidesPerView: swiperView,
            spaceBetween: 15,
            grabCursor: true
            //preloadImages: false,
            // Enable lazy loading
            //lazyLoading: true
        });

        var foodGroup = new Swiper('.food-swiper', {
            /*scrollbar: '.swiper-scrollbar',
            scrollbarHide: true,*/
            slidesPerView: 'auto',
            spaceBetween: 15,
            grabCursor: true
            //preloadImages: false,
            // Enable lazy loading
            //lazyLoading: true
        });

        /*-------------------------------------*/
        /*	FORM VALIDATION
         /*-------------------------------------*/
        $("#book-table").validate({
            rules:{
                cust_name:{required: true},
                cust_email:{required: true,email:true},
                cust_phone:{required: true,number:true,minlength:8},
                timepicker:{required: true},
                datepicker:{required: true}
            },
            submitHandler: function (form) {
                var data = [],
                        suburl = 'mail.html',
                        dine=$("#personNo").val(),
                        occasionType=$("#occasionType").val(),
                        food_pref=$("#food_pref").val(),
                        cust_name=$("#cust_name").val(),
                        cust_phone=$("#cust_phone").val(),
                        cust_email=$("#cust_email").val(),
                        timepicker=$("#timepicker").val(),
                        datepicker=$("#datepicker").val();

                $('#book-table .form-message').show();

                var data={'formid':'book_table','dine':dine,'occasiontype':occasionType,'food_pref':food_pref,'cust_name':cust_name,'cust_phone':cust_phone,'cust_email':cust_email,'tp':timepicker,'dp':datepicker};
                $.post(suburl ,  data , function(response){
                    $('.final_message').html(response);
                    $('.final_message').show();

                    $( '#book-table' ).each(function(){
                        this.reset(); //CLEARS THE FORM AFTER SUBMISSION
                    });
                });

                return false;
            }
        });

        $("#contact-us-form").validate({
            rules:{
                contactName:{required: true},
                contactEmail:{required: true,email:true},
                contactComment:{required: true}
            },
            submitHandler: function (form) {

                var suburl = 'mail.html',
                        contactName=$("#contactName").val(),
                        contactEmail=$("#contactEmail").val(),
                        contactComment=$("#contactComment").val();

                $('#contact-us-form .form-message').show();

                var data={'formid':'contact-us-form','cust_name':contactName,'comments':contactComment,'cust_email':contactEmail};

                $.post(suburl ,  data , function(response){
                    $('.contact-form-msg').html(response);
                    $('.contact-form-msg').show();

                    $( '#book-table' ).each(function(){
                        this.reset(); //CLEARS THE FORM AFTER SUBMISSION
                    });
                });

                return false; }
        });

        $("#home-newsletter").validate({
            rules:{
                newsEmail:{required: true,email:true}
            },
            submitHandler: function (form) {
                var newsEmail=$("#newsEmail").val();
                $('#home-newsletter .form-message').show();
                var data={email:newsEmail};
                $.post('check.html' , data , function(response){
                    $('#home-newsletter .form-message').html(response);
                    $('#home-newsletter .form-message').show();

                    $('#home-newsletter').each(function(){
                        this.reset(); //CLEARS THE FORM AFTER SUBMISSION
                    });
                });

                return false; }
        });

        /*--------------------------*/
        /*	Parallax Background
         /*--------------------------*/
        $(window).stellar();
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".owl-carousel").owlCarousel({
            loop:true,
            autoWidth:true,
            autoplay: true,
            margin: 10
        });
    });

    $(document).ready(function(){
        'use strict';
        /*----------------------------------------------------*/
        /*	GALLERY PAGE FANCYBOX
         /*----------------------------------------------------*/
        $('.fancybox').fancybox(
                {closeBtn  : true,
                    helpers   : { overlay : {closeClick: true} // prevents closing when clicking OUTSIDE fancybox
                    }
                    /* keys : { close  : null }*/
                });
    });
</script>

<script>
    var mainmenu = $('.main-menu');
    var cart = mainmenu.find('.dropdown-cart-div');

    $('html').click(function () {
        var nav_account = $('.dropdown-acc-div');
        if (cart.hasClass('open')) cart.removeClass('open');
        if (nav_account.hasClass('open')) nav_account.removeClass('open');
    });

    $('.dropdown-cart-div').click(function (event) {
        event.stopPropagation();
    });

    $('.dropdown-acc-div').click(function (event) {
        event.stopPropagation();
    });

    $('.dropdown-cart').click(function (event) {
        var wrap = $(this).parents('ul').find('.dropdown-acc-div');
        //Stop propagation
        event.stopPropagation();
        (cart.hasClass('open')) ? cart.removeClass('open') : cart.addClass('open');
        (wrap.hasClass('open')) ? wrap.removeClass('open') : '';
    });

    $('.dropdown-acc').click(function (event) {
        var wrap = $(this).find('.dropdown-acc-div');
        //Stop propagation
        event.stopPropagation();
        event.preventDefault();
        (wrap.hasClass('open')) ? wrap.removeClass('open') : wrap.addClass('open');
        (cart.hasClass('open')) ? cart.removeClass('open') : '';
    });

    $('.add-cart').click(function (event)
    {
        event.preventDefault();

        var cart = $(this),
                productId = cart.data('skuld'),
                amount = cart.data('amount'),
                product = cart.data('product'),
                price = cart.data('price'),
                wrapItemCart = $('.totalItem'),
                countItemCart = $('#totalItem').text(),
                listItems = $('#ListItems');

        var url = "{{url('carts/add')}}";

        swal({
                title: "",
                text: "Apakah Anda yakin untuk menambahkan " + product + " ke cart?",
                imageUrl: "{{asset('assets/public/images/food-gallery/appetizers/appetizers_01.jpg')}}",
                showCancelButton: true,
                confirmButtonText: "Ya!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    var products = {};
                    $.post(url, {product_id: productId, amount: amount}).done(function (data) {
                        if (data) console.log(JSON.stringify(data));
                        products = data;
                        //console.log(data.item);
                        $('#SubTotal').text('Rp ' + data.subtotal);

                        var skuld = $('#skuld'+productId);
                        if (skuld.length)
                        {
                            skuld.find('.amount').text(data.item.amount);
                        }
                        else
                        {
                            var sku = "skuld"+productId;
                            var item= '<li class="cart-item" id=' + sku + '>'
                                    + '<div class="img">'
                                    + '<img src="{{asset('assets/public/products/3.jpg')}}" class="img-responsive"/>'
                                    + '</div>'
                                    + '<div class="desc">'
                                    + '<div>'+ product +'</div>'
                                    + '<div class="sm"><span class="amount">' + amount + '</span> x Rp ' + data.item.extra.display_price + '</div>'
                                    + '</div>'
                                    + '</li>';

                            listItems.append(item);
                        }
                    });

                    var totalItem = parseInt(countItemCart)+amount;

                    wrapItemCart.text(totalItem);
                    $('.cart-message').hide();

                    swal("Berhasil", product + " berhasil di tambahkan ke cart", "success");
                }
                else {
                    swal("", "Anda membatalkan " + product + " ke cart", "error");
                }
            }
        );

    });

    $('.apply-voucher').click(function () {

        var voucher = $('input[name="voucher"]').val();
        var url = "{{url('voucher','verify')}}";
        var parent = $('.gift-cert');
        var total = $('.cart-confirmation-info-total');

        $.post(url, {code: voucher}).done(function (data) {
            if (data.response)
            {
                var cont = '<div class="item"><div class="labels text-info">'+ voucher +'</div><div class="value">- Rp '+ data.debit +'</div></div>';
                console.log(JSON.stringify(data));
                parent.before(cont);
                total.find('.value').text('Rp ' + data.total);
                parent.fadeOut('slow');
            }
            console.log(JSON.stringify(data));
        })
    });

    $('.lunch-box-wrapper').pin({
        containerSelector: ".food-categories",
        padding: {top: 80},
        minWidth: 1007
    });

    $('.cart-confirmation-col.pinned').pin({
        containerSelector: ".cart-wrapper",
        padding: {top: 80},
        minWidth: 1007
    });

    $('.acc-points-div.pinned').pin({
        containerSelector: ".acc-cont",
        padding: {top: 80},
        minWidth: 1007
    });

    /* Checkout Step */
    $('.btn-cart-step').click(function () {
        var step = $(this).data('step'),
                openStep = $('.cart-checkout-step-'+step),
                current = $(this).closest('.cart-checkout-step');

        current.hide('slow');
        openStep.show('slow');

        switch (step)
        {
            case 5 :
                scrollToElement(openStep,2000,-520);
                break;
            case 4 :
                scrollToElement(openStep,2000,-350);
                break;
            case 3 :
                scrollToElement(openStep,2000,-450);
                break;
        }
    });

    function scrollToElement(selector, time, verticalOffset) {
        time = typeof(time) != 'undefined' ? time : 1000;
        verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
        element = $(selector);
        offset = element.offset();
        offsetTop = offset.top + verticalOffset;
        $('html, body').animate({
            scrollTop: offsetTop
        }, time);
    }

    function formatNumber(n) {
        return n.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '$1.');
    }

    $('.ajax-add-item').click(function(e)
    {
        //prevent default action
        e.preventDefault();
        
        var url = $(this).attr('href');
        var method = $(this).data('method');
        var item = $(this).data('item');
        
        $.post(url, {item: item}, function(data){
            if(data){
                var lunchbox = $('.item-lunch-box');
                var item = "<li><div>"+data.product+"</div><div>"+data.point+" Point</div></li>";
                lunchbox.append(item);
            }
        })
    });
</script>

<script>       

    $(document).ready(function(){
        'use strict';
        /* ==============================================
        FOOD GALLERY PAGE ISOTOPE
        =============================================== */

        var $container = jQuery('#food-items');

        $container.isotope({
            itemSelector : '.food-item-wrapper',
            animationOptions: {
                 duration: 750,
                 easing: 'linear',
                 queue: false
               }
        });
          
        $('.food-type-list li a').on( 'click',  function(e) {
            e.preventDefault();
            var filterValue = $(this).attr('data-filter');
            $container.isotope({ filter: filterValue });
        });
    });

</script>