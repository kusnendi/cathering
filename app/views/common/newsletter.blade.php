<section class="news-letter">
    <div class="container clearfix">
        <article>
            <h3>be always updated with us</h3>
            <h4>Sign in with our newsletter</h4>
        </article>
        <article>
            <form id="home-newsletter">
                <input id="newsEmail" type="text" placeholder="Leave your mail, we will get back to you :)">
                <button><i class="fa fa-arrow-right"></i></button>
                <div class="form-message">
                    <div><div class="loader">Loading...</div></div>
                </div>
            </form>
        </article>
    </div>
</section>