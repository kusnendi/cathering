<section class="everyday-events">
    <div class="container">
        <img src="assets/public/images/booking-icon.png" alt="">
        <h2>everyday events</h2>
        <h4>all upcoming events</h4>

        <!-- ============ FEATURED EVENTS ================== -->
        <div class="feat-event">
            <div class="feature-events-wrapper">
                <div class="feature-events clearfix">
                    <div class="figure">
                        <div class="imgLiquidFill imgLiquid" >
                            <img src="assets/public/images/featured-event1.jpg" alt="" >
                            <div class="shine"></div>
                        </div>
                    </div>

                    <div class="figcaption event">

                        <div class="corner-details">
                            <div class="corner-date">Mar 15, 2014</div>
                            <div class="corner-time"><i class="fa fa-clock-o"></i>12pm - 4pm</div>
                        </div>

                        <h3>Poultry in Motion</h3>

                        <p>Cheese triangles cow feta. Squirty cheese red leicester monterey jack the big cheese danish fontina caerphilly cheesy feet cheese strings. Cow cheese slices melted cheese edam lancashire blue</p>

                        <a class="button" href="#">READ MORE</a>
                    </div>
                </div>
            </div>

            <div class="feature-events-wrapper">
                <div class="feature-events clearfix">
                    <div class="figure">
                        <div class="imgLiquidFill imgLiquid" >
                            <img src="assets/public/images/featured-event2.jpg" alt="" >
                            <div class="shine"></div>
                        </div>
                    </div>

                    <div class="figcaption event">
                        <!-- <div class="corner-date">
                            Mar 15, 2014
                        </div> -->
                        <div class="corner-details">
                            <div class="corner-date">Mar 15, 2014</div>
                            <div class="corner-time"><i class="fa fa-clock-o"></i>10am - 1pm</div>
                        </div>

                        <h3>Wok &amp; Roll</h3>

                        <p>Boursin camembert de normandie halloumi. Bavarian bergkase ricotta manchego cheddar bocconcini cheesy feet cheese strings brie. St. agur blue cheese port-salut danish fontina cheese strings fondue mascarpone queso cheesy feet</p>

                        <a class="button" href="#">READ MORE</a>
                    </div>
                </div>
            </div>

            <div class="feature-events-wrapper">
                <div class="feature-events clearfix">
                    <div class="figure">
                        <div class="imgLiquidFill imgLiquid" >
                            <img src="assets/public/images/featured-event3.jpg" alt="" >
                            <div class="shine"></div>
                        </div>
                    </div>

                    <div class="figcaption event">
                        <!-- <div class="corner-date">
                            Mar 15, 2014
                        </div> -->
                        <div class="corner-details">
                            <div class="corner-date">Mar 15, 2014</div>
                            <div class="corner-time"><i class="fa fa-clock-o"></i>12pm - 4pm</div>
                        </div>

                        <h3>Thai Tanic</h3>

                        <p>Pepper jack roquefort taleggio. Camembert de normandie roquefort bavarian bergkase squirty cheese airedale stinking bishop fromage frais fromage. Smelly cheese fromage frais cut the cheese </p>

                        <a class="button" href="#">READ MORE</a>
                    </div>
                </div>
            </div>

            <div class="feature-events-wrapper">
                <div class="feature-events clearfix">
                    <div class="figure">
                        <div class="imgLiquidFill imgLiquid" >
                            <img src="assets/public/images/featured-event4.jpg" alt="" >
                            <div class="shine"></div>
                        </div>
                    </div>

                    <div class="figcaption event">
                        <!-- <div class="corner-date">
                            Mar 15, 2014
                        </div> -->
                        <div class="corner-details">
                            <div class="corner-date">Mar 15, 2014</div>
                            <div class="corner-time"><i class="fa fa-clock-o"></i>2pm - 4pm</div>
                        </div>

                        <h3>Assaulted Peanuts</h3>

                        <p>Brie cauliflower cheese boursin. Pepper jack boursin edam roquefort queso cheese slices jarlsberg everyone loves. Croque monsieur when the cheese comes out everybody's happy danish</p>

                        <a class="button" href="#">READ MORE</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- =============================================== -->
        <a href="#" class="button white-btn">LOAD MORE</a>

    </div>
</section>