<div class="overlay">
    <div class="modal login-page">
        <i class="fa fa-times"></i>

        <a href="index-old.html" class="logo">
            <h1>AWESOME SPICES</h1>
        </a>

        <h2>sign up now</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>
        <div class="clearfix existing-login">
            <div class="fb-login-wrapper">
                <a href="#" class="fb-login button clearfix">
                    <i class="fa fa-facebook"></i>
                    <span>login with facebook</span>
                </a>
            </div>

            <div class="twitter-login-wrapper">
                <a href="#" class="twitter-login button clearfix">
                    <i class="fa fa-twitter"></i>
                    <span>login with twitter</span>
                </a>
            </div>
        </div>

        <h3>Already have an account? Login now</h3>
        <form>
            <div class="clearfix">
                <div class="input-wrapper user-name">
                    <input type="text" placeholder="Your Name">
                </div>
                <div class="input-wrapper pass">
                    <input type="text" placeholder="Password">
                </div>
            </div>
            <div class="clearfix form-btn-wrapper">
                <button class="button red-btn">login</button>
                <h5>Forgot Password? Click <a href="#">here</a></h5>
            </div>
        </form>

    </div> <!-- LOGIN MODAL ends -->
</div> <!-- OVERLAY ends -->