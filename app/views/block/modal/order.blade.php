<div class="overlay">
    <div class="modal order-page">
        <i class="fa fa-times"></i>
        <div class="head clearfix">
            <div class="page-name">
                <h3>order page</h3>
            </div>
            <div class="order-date-details">
                <h5>Your order ID is <span id="orderid">AQ19873562KP</span> </h5>
                <h6 id="order-date">Wednesday, 28th May 2014</h6>
            </div>
        </div>
        <div class="sub-head clearfix">
            <div class="captn">
                <h6>Your selected items are listed below</h6>
            </div>
            <div class="no-of-dishes">
                <h6>You have selected <span class="selected-dishes-no-pop">0</span> dishes.</h6>
                <div>
                    <img src="_assets/images/chef-hat-icon-grey.png" height="40" width="41" alt="">
                    <span class="selected-dishes-no-pop">0</span>
                </div>
            </div>
        </div>
        <div class="items-wrapper">
            <!-- ============= HEADER ================== -->
            <div class="order-pg-items tophead clearfix">
                <div class="item-details-price clearfix">
                    <div class="order-item-details clearfix">
                        <div class="figcap">
                            <h4>Item Name</h4>
                        </div>
                        <div class="figcap rate">
                            <h4>Rate</h4>
                        </div>
                    </div>
                    <div class="order-item-price clearfix">
                        <h4>Price</h2>
                            <h4>Quantity</h2>
                    </div>
                </div>
            </div>

            <div id="ordered-items"></div>

        </div>

        <div class="order-value clearfix">
            <div class="total-value-details">
                <h4>Your total value is</h4>
                <h5>After adding all tax</h5>
                <div class="discount-promo">
                    <h5>Use discount promo</h5>
                    <span>qwe2554er</span>
                </div>
            </div>
            <div id="total_amount" class="total-value">
                <input type="text" class="hidden-field" value="" />
                <h2>$ 00.00</h2>
            </div>
        </div>

        <form id="order-form">

            <div id="dishes"></div>
            <div class="info-address">
                <h3>your info and address</h3>
                <div class="clearfix outer-wrapper">
                    <div class="input-wrapper">
                        <input type="text" id="orderName" name="orderName" placeholder="Your name">
                    </div>
                    <div class="input-wrapper">
                        <input type="text" id="orderEmail" name="orderEmail" placeholder="Email Id">
                    </div>
                </div>
                <div class="clearfix outer-wrapper">
                    <div class="input-wrapper">
                        <input type="text" id="orderAddress" name="orderAddress" placeholder="Write your full address">
                    </div>
                    <div class="clearfix half">
                        <div class="input-wrapper">
                            <input type="text" id="orderPincode" name="orderPincode" placeholder="Zip code">
                        </div>
                        <div class="input-wrapper">
                            <input type="text" id="orderPhone" name="orderPhone" placeholder="Phone no">
                        </div>
                    </div>
                </div>
            </div>
            <div class="final-order clearfix">
                <button class="button green-btn">order now</button>
                <div class="order-msg form-message" >
                    <div><div class="loader">Loading...</div></div>
                </div>
            </div>
        </form>

    </div>
    <!-- MODAL ends -->
</div>