<footer>
    <div class="container">
        <div class="footer-wrap">
            <div class="wrap">
                <div class="row hidden-xs">
                    <div class="col-md-3">
                        <div class="header">Company</div>
                        <ul class="nav">
                            <li><a href="{{url('about-us')}}">About Us</a></li>
                            <li><a href="#">Terms &amp; Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li style="display: none;">Career</li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <div class="header">Help &amp; Information</div>
                        <ul class="nav">
                            <li><a href="#">Delivery Coverage</a></li>
                            <li><a href="#">Payment</a></li>
                            <li><a href="#">Account</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <div class="header">Stay Connected</div>
                        <ul class="nav">
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Instagram</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <div class="header">Contact Us</div>
                        <div class="text-grey">
                            Mon - Fri : 9 am - 6 pm<br/>
                            Sat - Sun : Off<br/>
                            Tel. +62 21 1233 4455<br/>
                            <a href="mailto:customer@mealtime.id" style="display: none;">customer@mealtime.id</a>
                        </div>
                    </div>
                </div>
                <div class="copyright text-center text-grey">&copy; MealTime {{(date('Y') != 2016) ? '2016 - ' . date('Y') : date('Y')}}. All rights reserved.</div>
            </div>
        </div>
    </div>
</footer>