<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="{{asset('resources/banner/banner1.jpg')}}" alt="...">
        </div>
        <div class="item">
            <img src="{{asset('resources/banner/banner2.jpg')}}" alt="...">
        </div>
    </div>
</div>