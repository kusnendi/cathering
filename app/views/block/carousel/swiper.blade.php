<!-- Slider main container -->
<div class="swiper-container">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        @foreach($items as $item)
        <div class="swiper-slide">
            <div class="food-card">
                <div class="thumbnail food-card">
                    <img src="{{asset($item->image)}}" alt="...">
                    <div class="caption">
                        <div class="tag-featured">
                            <img src="{{asset('resources/icons/tag-spicy.png')}}" alt="Tag" />
                        </div>
                        <h3>{{str_limit($item->product,20)}}</h3>
                        <div class="food-meta">
                            <div class="food-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-empty"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <div class="food-point">
                                {{$item->point}} Point
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>
    <!-- Add Scrollbar -->
    <div class="swiper-scrollbar"></div>
</div>