<section class="food-solutions">
    <div class="container">
        <img src="assets/public/images/food-solution-icon.png" alt="">
        <h2>complete your day</h2>
        <h4>all day food solutions</h4>

        <!-- ============= FOOD MENUS ================== -->
        <div class="food-menus clearfix">
            <figure>
                <img src="assets/public/images/food-img.jpg" alt="">
            </figure>
            <div class="figcaption">
                <div class="food-menu-heading">
                    <h1>Breakfast</h1>
                    <h6>Fresh and healthy breakfast available from 7am</h6>
                </div>


                <div class="menu-items-list clearfix">

                    <ul class="bxslider a clearfix">
                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">
                                    <h2>Apple Crumb Squares</h2>

                                    <h5>$ 12.49</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item2.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Red Velvet Cupcakes</h2>
                                    <h5>$ 82.49</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item3.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Vanilla Crème Brûlée</h2>
                                    <h5>$ 22.89</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Pear-Cranberry Lattice Pie</h2>
                                    <h5>$ 25.75</h5>
                                </div>
                            </article>
                        </li>

                    </ul>
                </div>

                <div class="direction-btns">
                    <a class="prev-btn" href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="next-btn" href="#">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

                <div class="cust-rating">
                    <h5>8.5</h5>
                    <h6>Customer review</h6>
                </div>
            </div>
        </div>


        <div class="food-menus clearfix">
            <figure>
                <img src="assets/public/images/food-img2.jpg" alt="">
            </figure>
            <div class="figcaption">
                <div class="food-menu-heading">
                    <h1>Lunch</h1>
                    <h6>Fresh and healthy breakfast available from 7am</h6>
                </div>

                <div class="menu-items-list clearfix">

                    <ul class="bxslider1 clearfix">
                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item3.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Glazed Carrot Cake</h2>
                                    <h5>$ 15.26</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item2.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Praline-Iced Brownies</h2>
                                    <h5>$ 75.24</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Lemon Ricotta Cheesecake</h2>
                                    <h5>$ 62.58</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item3.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Nutty Popcorn Fudge</h2>
                                    <h5>$ 63.21</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item2.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Whoopie Pies</h2>
                                    <h5>$ 52.10</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Blueberry-Buttermilk Coffee Cake</h2>
                                    <h5>$ 54.98</h5>
                                </div>
                            </article>
                        </li>

                    </ul>
                </div>

                <div class="direction-btns">
                    <a class="prev-btn" href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="next-btn" href="#">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

                <div class="cust-rating">
                    <h5>8.5</h5>
                    <h6>Customer review</h6>
                </div>
            </div>
        </div>


        <div class="food-menus clearfix">
            <figure>
                <img src="assets/public/images/food-img3.jpg" alt="">
            </figure>

            <div class="figcaption">
                <div class="food-menu-heading">
                    <h1>Dinner</h1>
                    <h6>Fresh and healthy breakfast available from 7am</h6>
                </div>


                <div class="menu-items-list clearfix">

                    <ul class="bxslider2 clearfix">
                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item2.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Chocolate Peppermint Ice Cream Cake</h2>
                                    <h5>$ 23.25</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Pumpkin Tiramisu</h2>
                                    <h5>$ 32.49</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item3.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Black-Bottom Peanut Pie</h2>
                                    <h5>$ 2.49</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item2.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Spiced Apple Pie with Cheddar Crust</h2>
                                    <h5>$ 19.49</h5>
                                </div>
                            </article>
                        </li>

                        <li class="clearfix">
                            <article class="menu-items">
                                <figure>
                                    <img src="assets/public/images/menu-item.jpg" alt="">
                                </figure>
                                <div class="figcaption">

                                    <h2>Bourbon-Pecan Pie</h2>
                                    <h5>$ 15.49</h5>
                                </div>
                            </article>
                        </li>

                    </ul>
                </div>

                <div class="direction-btns">
                    <a class="prev-btn" href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="next-btn" href="#">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

                <div class="cust-rating">
                    <h5>8.5</h5>
                    <h6>Customer review</h6>
                </div>
            </div>
        </div>
        <!-- ============================================ -->
    </div>
</section>