<section class="user-reviews">
    <div class="container">
        <img src="assets/public/images/booking-icon.png" alt="">
        <h2>user reviews</h2>
        <h4>our valuable users' review</h4>
        <!-- ============= REVIEWS ================== -->
        <article class="review clearfix">
            <figure class="imgLiquid imgLiquidFill">
                <!-- <i class="fa fa-user"></i> -->
                <img src="assets/public/images/reviewer.png" alt="">
            </figure>
            <div class="figcaption">
                <h5>Micheles Doe</h5>
                <h6>Architect, California</h6>
                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire</p>
            </div>
        </article>


        <article class="review clearfix">
            <figure class="imgLiquid imgLiquidFill">
                <!-- <i class="fa fa-user"></i> -->
                <img src="assets/public/images/reviewer2.png" alt="">
            </figure>
            <div class="figcaption">
                <h5>Gerard Doe</h5>
                <h6>Artist, Utah</h6>
                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire</p>
            </div> <!-- FIGCAPTION ends -->
        </article>

        <article class="review clearfix">
            <figure class="imgLiquid imgLiquidFill">
                <!-- <i class="fa fa-user"></i> -->
                <img src="assets/public/images/reviewer3.png" alt="">
            </figure>
            <div class="figcaption">
                <h5>Micheles Butler</h5>
                <h6>Scientist, Berlin</h6>
                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire</p>
            </div> <!-- FIGCAPTION ends -->
        </article>
        <!-- ============================================ -->
    </div>
</section>