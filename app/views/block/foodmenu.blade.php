<article class="search-menu-list lnch">
    <div class="head">
        <img src="assets/public/images/food-menu-icon.png" alt="">
        <h2>lunch</h2>
        <h6>Your search results listed below</h6>
    </div>
    <div class="menu-items-wrapper" >
        <ul class="lnchSlider clearfix">
            <!-- ============= SEARCH-MENU-LIST =============== -->
            <li data-name='Pasta e Fagioli' data-price='22.58' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item5.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Pasta e Fagioli</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$22.58</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Meat Lasagna' data-price='21.13' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item3.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Meat Lasagna</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$21.13</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Rigatoni with Beef' data-price='49.35' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item4.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Rigatoni with Beef</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$49.35</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Chicken Cacciatore' data-price='64.89' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item9.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Chicken Cacciatore</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$64.89</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Arroz con Pollo' data-price='11.57' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item8.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Arroz con Pollo</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$11.57</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Rosemary Chicken' data-price='09.26' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item7.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Rosemary Chicken</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$09.26</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Chopped Greek Salad' data-price='14.51' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item2.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Chopped Greek Salad</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$14.51</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Fresh Salmon Tacos' data-price='24.13' class="own">
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Fresh Salmon Tacos</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$24.13</h2>
                        </div>
                    </div>
                </div>
            </li>
            <!-- ============================================== -->
        </ul>
        <div class="nav-btns">
            <a class="left-btn" href="javascript:void(0)"><i class="fa fa-angle-left"></i></a>
            <a class="right-btn" href="javascript:void(0)"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</article>

<article class="search-menu-list dinr">
    <div class="head">
        <img src="assets/public/images/food-menu-icon.png" alt="">
        <h2>dinner</h2>
        <h6>Your search results listed below</h6>
    </div>
    <div class="menu-items-wrapper" >
        <ul class="dnnrSlider clearfix">
            <!-- ============= SEARCH-MENU-LIST =============== -->
            <li data-name='Mexican Grilled Corn' data-price='18.79'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item3.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Mexican Grilled Corn</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$18.79</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Guiltless Guacamole' data-price='35.67'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item8.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Guiltless Guacamole</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$35.67</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Tortilla Soup' data-price='26.45'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item9.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Tortilla Soup</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$26.45</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Oregon Tuna Melts' data-price='51.84'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item5.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Oregon Tuna Melts</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$51.84</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Hum Yum' data-price='63.34'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item6.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Hum Yum</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$63.34</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Turkey Pastrami' data-price='43.44'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item7.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Turkey Pastrami </h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$43.44</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Salami and Cheese' data-price='82.91'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item4.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Salami and Cheese</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$82.91</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Red-Cooked Pork' data-price='13.43'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/search-food-item.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Red-Cooked Pork</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$13.43</h2>
                        </div>
                    </div>
                </div>
            </li>
            <!-- ============================================== -->
        </ul>
        <div class="nav-btns">
            <a class="left-btn" href="javascript:void(0)"><i class="fa fa-angle-left"></i></a>
            <a class="right-btn" href="javascript:void(0)"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</article>

<!-- ================================================= -->
<article class="search-menu-list drinks">
    <div class="head">
        <img src="assets/public/images/food-menu-icon.png" alt="">
        <h2>drinks</h2>
        <h6>Your search results listed below</h6>
    </div>
    <div class="menu-items-wrapper" >
        <ul class="bxslider drnkSlider clearfix">
            <!-- ============= SEARCH-MENU-LIST =============== -->
            <li data-name='Perfect Punches' data-price='41.33'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item2.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Perfect Punches</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$41.33</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Homemade Lemonade' data-price='24.15'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item4.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Homemade Lemonade </h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$24.15</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Low-Cal Drinks' data-price='44.56'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item1.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Low-Cal Drinks</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$44.56</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Healthy Smoothies' data-price='62.35'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item3.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Healthy Smoothies</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$62.35</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Tropical Gin Fizz' data-price='13.24'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item4.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Tropical Gin Fizz</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$13.24</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Breakfast Martini' data-price='41.99'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item1.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Breakfast Martini</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$41.99</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Lillet-Basil Cocktail' data-price='36.25'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item3.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Lillet-Basil Cocktail</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$36.25</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Tinto de Primavera' data-price='16.46'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item4.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Tinto de Primavera</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$16.46</h2>
                        </div>
                    </div>
                </div>
            </li>

            <li data-name='Mint Frappes' data-price='23.45'>
                <div class="search-menu-items clearfix">
                    <figure>
                        <img src="assets/public/images/drink-item1.jpg" alt="">
                    </figure>
                    <div class="figcaption clearfix">
                        <div>
                            <h3>Mint Frappes</h3>
                            <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <div>
                                <h6>Ingredients</h6>
                                <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                                <h6>Customer Review</h6>
                                <div class="cust-rating" >
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="red fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="price-add-select clearfix">
                            <a class="button white-btn clicked" href="javascript:void(0)">
                                <span class="desk">add to list</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button green-btn" href="javascript:void(0)">
                                <span class="desk">selected</span>
                                <span class="mob"><i class="fa fa-check"></i></span>
                            </a>
                            <a class="button red-btn" href="javascript:void(0)">X</a>
                            <h2>$23.45</h2>
                        </div>
                    </div>
                </div>
            </li>
            <!-- ============================================== -->
        </ul>
        <div class="nav-btns">
            <a class="left-btn" href="javascript:void(0)"><i class="fa fa-angle-left"></i></a>
            <a class="right-btn" href="javascript:void(0)"><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</article>