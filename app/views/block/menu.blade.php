<!-- ========= DESKTOP NAV MENU ========== -->
<nav id="navigation-menu" class="nav-menu navbar-collapse" role="navigation" style="display: none;">

    <!-- ========== MOBILE MENU ================ -->
    <div id="nav-div" class="clearfix">
        <div class="navbar-header">
            <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target=".nav-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <nav id="navigation-list" class="row navbar-collapse"  role="navigation">
            <ul class="nav navbar-nav">
                <li class=""><a href="/">CATERING</a></li>
                <li class=""><a href="{{URL::to('menu')}}">READY TO EAT</a></li>
                <li class=""><a href="{{URL::to('contact-us')}}">CONTACT US</a></li>
                <li class=""><a href="{{URL::to('register')}}">REGISTER</a></li>
                <li class=""><a href="{{URL::to('login')}}">LOGIN</a></li>
            </ul>
        </nav>
    </div> <!-- ========== MOBILE MENU ends ============ -->

</nav>

<div class="main-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-8" style="padding-left: 0px">
                <ul class="nav nav-inline">
                    <li class=""><a href="/">CATERING</a></li>
                    <li class="" style="display: none;"><a href="{{URL::to('menu')}}">READY TO EAT</a></li>
                    <li class=""><a href="{{URL::to('contact-us')}}">COORPORATE</a></li>
                    <li class="" style="display: none;"><a href="{{URL::to('contact-us')}}">BLOG</a></li>
                    @if(Auth::check())
                        
                    @endif
                    <li class=""><a href="{{URL::to('contact-us')}}">BANTUAN</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                @if(Auth::check())
                    <ul class="nav nav-inline menu">
                        <li class="acc"><a href="{{url('accounts')}}">Hi, {{Auth::user()->Profile->fullname}}</a></li>
                        <li class="pts"><a href="{{url('add-point')}}" class="dropdown-point"><span>{{(isset($points->point)) ? $points->point : 0}}</span> Point</a></li>
                        <li class="crt">
                            <a href="#" class="dropdown-cart">
                                <span id="totalItem" class="total-cart text-primary totalItem"><strong>{{$carts->count()}}</strong></span>
                                <span class="tx hidden-xs hidden-sm">Cart</span>
                            </a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>

        <div class="dropdown-cart-div">
            <div class="cart-header text-center">
                My Cart
            </div>

            <div class="cart-list" style="min-height: 170px">
                <?php $subtotal=0; ?>
                @if($carts->count())
                    <ul class="list-unstyled" id="ListItems">
                    @foreach($carts as $cart)
                        {{--*/ $subtotal=$subtotal + ($cart->amount*$cart->price) /*--}}
                        <li class="cart-item" id="skuld{{$cart->product_id}}">
                            <div class="img">
                                <img src="{{asset('assets/public/products/3.jpg')}}" class="img-responsive"/>
                            </div>
                            <div class="desc">
                                <div>{{$cart->Product->product}}</div>
                                <div class="sm"><span class="amount">{{$cart->amount}}</span> x Rp {{number_format($cart->price,0,',','.')}}</div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                @else
                    <p class="cart-message" style="font-size: smaller">Saat ini belum ada item dalam keranjang.</p>
                    <ul class="list-unstyled" id="ListItems"></ul>
                @endif
            </div>

            <div class="cart-subtotal">
                <div class="row">
                    <div class="col-xs-5 tx">Sub Total</div>
                    <div id="SubTotal" class="col-xs-7 text-right amount">Rp {{number_format($subtotal,0,',','.')}}</div>
                </div>
            </div>

            <div>
                <a href="{{URL::to('checkout')}}" class="btn btn-block btn-primary">Check Out</a>
            </div>
        </div>
    </div>
</div>
<!-- =============== DESKTOP NAV MENU ends ==================== -->