<div class="social-btns-group" style="display: none">
    <a class="mobile-social-btn" href="#"><i class="fa fa-share"></i></a>
    <!-- ========= SOCIAL BUTTONS ========== -->
    <ul class="social-btns clearfix">
        <li><a class="youtube" href="#"><i class="fa fa-youtube"></i></a></li>
        <li><a class="vimeo" href="#"><i class="fa fa-vimeo-square"></i></a></li>
        <!-- <li><a class="behance" href="#"><i class="fa fa-behance"></i></a></li> -->
        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
    </ul>
</div> <!-- ========== SOCIAL BUTTONS ENDS ============= -->

<!-- ========= DESKTOP NAV MENU ========== -->
<div class="social-btns-group">
    <nav class=""  role="navigation">
        <ul class="nav navbar-nav navbar-right">
            <li class=""><a class="" href="#">BANTUAN</a></li>
            <li class=""><a href="javascript:void(0)">REGISTER</a></li>
            <li class=""><a href="{{URL::to('menu')}}">LOGIN</a></li>
            <li class=""><a href="javascript:void(0)"><i class="fa fa-shopping-bag" style="padding-right: 5px"></i>CART</a></li>
        </ul>
    </nav>
</div>
<!-- =============== DESKTOP NAV MENU ends ==================== -->