<ul class="nav nav-inline menu">
    <li class="hidden-xs hidden-sm">
        <ul class="nav nav-inline link">
            <li style="display: none;"><a href="{{URL::to('contact-us')}}">Contact Us</a></li>
            @if(Auth::check())
                <li><a href="{{URL::to('add-point')}}">Buy Point</a></li>
                <li class="dropdown-acc">
                    <a href="{{url('accounts')}}" class="dropdown">Hi, {{Auth::user()->Profile->fullname}}</a>
                    <div class="dropdown-acc-div">
                        <div class="wrap">
                            <div class="header">Account</div>
                            <ul class="nav">
                                <li>{{link_to('accounts', 'My Profile')}}</li>
                                <li>{{link_to('accounts/change-password', 'Change Password')}}</li>
                            </ul>
                            <hr/>
                            <div class="header">Orders</div>
                            <ul class="nav">
                                <li>{{link_to('orders/payment-confirmation', 'Payment Confirmation')}}</li>
                                <li><a href="{{url('orders/history')}}">Order History</a> {{($n_order) ? '<label class="label label-info">'.$n_order.'</label>' : ''}}</li>
                                <li>{{link_to('orders/rating', 'Rating')}}</li>
                            </ul>
                        </div>
                        <div class="logout">
                            <div class="wrap">
                                {{link_to('logout', 'Logout')}}
                            </div>
                        </div>
                    </div>
                </li>
            @else
                <li><a href="{{URL::to('register')}}">Register</a></li>
                <li><a href="{{URL::to('login')}}">Login</a></li>
            @endif
        </ul>
    </li>
    <li>
        <a href="#" class="dropdown-cart">
            <span id="totalItem" class="total-cart text-primary totalItem"><strong>{{$carts->count()}}</strong></span>
            <span class="tx hidden-xs hidden-sm">Cart</span>
        </a>
    </li>
</ul>