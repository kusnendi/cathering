<header class="new-type3 header-type2">
    <div class="upper-part">
        <div class="container clearfix" style="display: none">
            <div class="head-contact">
                <h5>CALL US <span class="phone">085720858023</span></h5>
                <h5>Email us at: customer@mealtime.id</h5>
            </div>
            <a class="logo hidden-xs" href="#"><h1>MEALTIME.ID</h1></a>

            <div class="social-btns-group">
                <a class="mobile-social-btn" href="#"><i class="fa fa-share"></i></a>
                <ul class="social-btns clearfix">
                    <li><a class="youtube" href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
        </div>

        <div class="container clearfix">
            <div class="row">
                <div class="col-lg-5 col-xs-6">
                    <div class="logos" style="opacity: 0">
                        <a href="#">
                            <img src="{{asset('assets/public/logo.png')}}" class="img-responsive" alt="Mealtime" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-xs-6">
                    @include('block.header.menu.top')
                </div>
            </div>
        </div>
    </div>

    <div class="lower-part">
        <div class="container">
            @include('block.menu')
        </div>
    </div>
</header>