<header class="default header-type2">
    <div class="header-inner-wrapper">
        <div class="container clearfix">
            @include('block.menu')
            @include('block.brand')
            @include('block.right-nav')
        </div>
    </div>
</header>