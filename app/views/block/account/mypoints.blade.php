<div class="acc-points-div pinned">
    <h3>My Points</h3>
    <div class="acc-points">
        <div class="value">{{(isset($points->point)) ? $points->point : 0}}</div>
        <div class="labels">Points</div>
        <button type="button" class="btn btn-block btn-danger btn-lg" onclick="location='{{url("add-point")}}'" >Top Up Point</button>
    </div>
</div>