<div class="acc-nav pinned">
    <div class="header">
        <span>Account</span>
    </div>
    <ul class="nav">
        <li><a href="{{url('accounts')}}">Profile</a></li>
        <li><a href="{{url('accounts/change-password')}}">Change Password</a></li>
    </ul>

    <div class="header">
        <span>Orders</span>
    </div>
    <ul class="nav">
        <li><a href="{{url('orders/payment-confirmation')}}">Payment Confirmation</a></li>
        <li><a href="{{url('orders/history')}}">Order History</a> {{($n_order) ? '<label class="label label-info">'.$n_order.'</label>' : ''}}</li>
        <li><a href="#">Rating</a></li>
    </ul>
</div>