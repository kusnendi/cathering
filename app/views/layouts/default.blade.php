<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Mealtime Catering Solutions</title>
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    @include('common.styles')
</head>

<body>
<div class="inside-body-wrapper index-pg">
    <!-- ============ HEADER TYPE 2 ================== -->
    @include('block.header.style3')
    <!-- ============ HEADER TYPE 2 ends ================== -->

    <div class="wrapper">
        @yield('content')
    </div> <!-- WRAPPER ends -->

    <!-- ============ FOOTER ================== -->
    @include('block.footer')
    <!-- ============ FOOTER ================== -->
</div> <!-- inside-body-wrapper ends -->

@include('common.scripts')
</body>
</html>
	