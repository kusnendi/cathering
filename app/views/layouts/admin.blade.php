<!DOCTYPE html>
<html>
<head>
    <title>MEALTIME :: FOOD SOLUTIONS</title>
    <meta charset="utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    @include('admin.common.styles')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    {{--<script>tinymce.init({ selector:'.wysiwyg' });</script>--}}

    <style>
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle;
        }
        .item {
            display: table;
            width: 100%;
        }
        .item .labels {
            display: table-cell;
            width: 50%;
            vertical-align: top;
            text-align: right;
        }
        .item .value {
            display: table-cell;
            width: 50%;
            vertical-align: top;
            text-align: right;
            padding-right: 8px;
        }
        .item .labels.total,.item .value.total{
            font-size: 16px;
            text-transform: capitalize;
            font-weight: bold;
        }
        .order-confirm-info .item {
            line-height: 24px;
        }
        .order-confirm-info .item > div:first-child {
            text-align: right;
        }
        .label-help {
            /*display: block;*/
            padding: 0px 3px;
            font-size: 10px;
        }
    </style>
</head>

<body class="flat-blue">
<div class="app-container">
    <div class="loader-container text-center color-white">
        <div><i class="fa fa-spinner fa-pulse fa-3x"></i></div>
        <div>Loading</div>
    </div>
    <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
            <div class="container-fluid">
                @include('admin.block.header')
                @include('admin.block.navigation.top')
            </div>
        </nav>
        <!-- Navigation Menu -->
        @include('admin.block.menu')
        <!-- Main Content -->
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
    <footer class="app-footer">
        <div class="wrapper">
            <span class="pull-right">2.0 <a href=""><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright Kusnendi.Muhamad.
        </div>
    </footer>
    <div>
        <!-- Javascript Libs -->
        @include('admin.common.scripts')
    </div>
</div>
</body>
</html>