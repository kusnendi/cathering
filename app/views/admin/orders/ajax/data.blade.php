<table class="table table-hover">
    <thead>
    <tr>
        <th width="10%">IDs</th>
        <th width="15%">Tanggal</th>
        <th width="20%">Customer</th>
        <th width="15%">Phone</th>
        <th width="10%">Status</th>
        <th width="3%"></th>
        <th width="15%" class="text-right">Subtotal</th>
        <th width="15%" class="text-right">Total</th>
    </tr>
    </thead>

    <tbody>
    @if($orders->count())
        @foreach($orders as $order)
            <tr>
                <td>
                    <a href="{{url('admin/orders',['view',$order->id])}}" title="Lihat detail order #{{$order->id}}">#{{sprintf('%06s', $order->id)}}</a>
                    @if($order->status == 'W')<div class="label-help label label-success">Verifikasi Pembayaran</div>@endif
                </td>
                <td>{{date('d/m/Y, H:i', strtotime($order->created_at))}}</td>
                <td>{{$order->fullname}}</td>
                <td>{{$order->user->profile->phone}}</td>
                <td>
                    <div class="btn-group btn-group-xs">
                        <a class="btn btn-xs {{$status[$order->status]['btn']}} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{$status[$order->status]['name']}} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            @foreach($status as $key => $value)
                                @if($key == $order->status)
                                    <li class="dropdown-header">{{$value['name']}}</li>
                                @elseif($key != 'K')
                                    <li><a class="ajax" href="{{url(Request::segment(2)."/status")}}" data-status="{{$key}}" data-invoice="{{$order->id}}" data-method="post">{{ucfirst($value['name'])}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-gears"></i> <span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            @foreach($act_options as $action)
                                @if($action['name'] == 'hapus')
                                    <li><a class="ajax" href="{{url($action['action'])}}" data-id="{{$order->id}}">{{ucfirst($action['name'])}}</a></li>
                                @else
                                    <li><a class="" href="{{url($action['action'],$order->id)}}">{{ucfirst($action['name'])}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td class="text-right">Rp {{number_format($order->subtotal,2,',','.')}}</td>
                <td class="text-right">Rp {{number_format($order->total,2,',','.')}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div>
    {{$orders->appends(array_except(Input::query(),'page'))->links()}}
</div>