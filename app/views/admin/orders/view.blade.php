@extends('layouts.admin')

@section('content')
    <div class="side-body">
        <div class="page-main" style="padding-top: 30px">
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-xs-12" style="margin-bottom: 0px">
                            <div class="panel panel-default">
                                <div class="panel-heading"><i class="fa fa-user"></i> Informasi Pelanggan</div>
                                <div class="panel-body">
                                    <a href="#" title="Detail User">{{$order->user->profile->fullname}}</a><br/>
                                    <a href="mailto:{{$order->user->email}}" title="Kirim User Email">{{$order->user->email}}</a><br/>
                                    {{$order->user->profile->address}}, Jakarta<br/>
                                    {{$order->user->profile->phone}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12" style="margin-bottom: 0px">
                            <div class="panel panel-default">
                                <div class="panel-heading"><i class="fa fa-location-arrow"></i> Alamat Pengiriman</div>
                                <div class="panel-body">
                                    <a href="#" title="Detail User">{{$order->fullname}}</a><br/>
                                    {{$order->user->profile->address}}, Jakarta<br/>
                                    {{$order->user->profile->phone}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><i class="glyphicon glyphicon-credit-card"></i> Metode Pengiriman</div>
                                <div class="panel-body">
                                    Instant Delivery
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-list-alt"></i> Order #{{sprintf('%06s', $order->id)}}
                            <div class="panel-tool pull-right">
                                <div class="btn-group" style="position: absolute; top: 3px; right: 25px">
                                    <button type="button" class="btn {{$status[$order->status]['btn']}} btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{$status[$order->status]['name']}} <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        @foreach($status as $key => $value)
                                            @if($key == $order->status)
                                                <li class="dropdown-header">{{$value['name']}}</li>
                                            @elseif($key != 'K')
                                                <li><a class="ajax" href="{{url(Request::segment(2)."/status")}}" data-status="{{$key}}" data-invoice="{{$order->id}}" data-method="post">{{ucfirst($value['name'])}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="min-height: 393px">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th width="45%">Product</th>
                                    <th width="20%">Price</th>
                                    <th width="10%" class="text-center">Quantity</th>
                                    <th width="25%" class="text-right">Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order->details as $item)
                                    {{--*/ $extra = unserialize($item->extra); /*--}}
                                    <tr>
                                        <td>
                                            <div style="display: table; width: 100%;">
                                                <div style="display: table-cell; width: 10%; vertical-align: top">
                                                    <img style="height: 50px" class="img-responsive" src="{{asset('assets/public/images/food-gallery/appetizers/appetizers_03.jpg')}}" />
                                                </div>
                                                <div style="display: table-cell; width: 30%; vertical-align: middle">{{$extra['product']}}</div>
                                            </div>
                                        </td>
                                        <td>Rp {{number_format($item->price,2,',','.')}}</td>
                                        <td class="text-center">{{$item->amount}}</td>
                                        <td class="text-right">Rp {{number_format($item->price*$item->amount,2,',','.')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <div class="item">
                                        <div class="labels">Subtotal:</div>
                                        <div class="value">Rp {{number_format($order->subtotal,2,',','.')}}</div>
                                    </div>
                                    @if($order->coupons)
                                        <div class="item">
                                            <div class="labels"><small><strong>Gift Voucher</strong></small></div>
                                            <div class="value"></div>
                                        </div>
                                        <div class="item">
                                            <div class="labels text-success">{{$order->coupons->coupon->code}}</div>
                                            <div class="value text-success">- Rp {{number_format($order->coupons->amount-$order->coupons->debit,2,',','.')}}</div>
                                        </div>
                                    @endif
                                    <div class="item" style="padding-top: 15px">
                                        <div class="labels total">Total:</div>
                                        <div class="value total text-danger">Rp {{number_format($order->total,2,',','.')}}</div>
                                    </div>
                                </div>
                            </div>
                            <div>Catatan:</div>
                            <div class="well well-sm">
                                <div class="">
                                    {{$order->notes}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="glyphicon glyphicon-th"></i> Action Tools</div>
                        <div class="panel-body">
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" onclick="javascript:window.open('{{URL::to("admin/orders/invoice/" . $order->id . "?action=print")}}')"><i class="glyphicon glyphicon-print"></i> Print</button>
                                <button class="btn btn-default" onclick="javascript:window.open('{{url("admin/orders/invoice",$order->id)}}')"><i class="fa fa-file-text-o"></i> Invoice</button>
                                <button class="btn btn-default"><i class="fa fa-file-pdf-o"></i> PDF</button>
                                <button class="btn btn-default disabled"><i class="fa fa-pencil"></i> Edit</button>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="glyphicon glyphicon-credit-card"></i> Informasi Pembayaran</div>
                        <div class="panel-body">
                            @if($order->payment_id == 0)
                                Gift Voucher
                            @elseif($order->payment_id == 2)
                                Bank Transfer
                            @endif
                        </div>
                    </div>

                    @if($order->confirmation)
                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-credit-card"></i> Konfirmasi Pembayaran</div>
                            <div class="panel-body">
                                <div class="order-confirm-info">
                                    <div class="item">
                                        <div class="lebels">Pemilik Rekening :</div>
                                        <div class="value">{{$order->confirmation->sender}}</div>
                                    </div>
                                    <div class="item">
                                        <div class="lebels">Tanggal Transfer :</div>
                                        <div class="value">{{date('d F Y', $order->confirmation->date)}}</div>
                                    </div>
                                    <div class="item">
                                        <div class="lebels">Jumlah :</div>
                                        <div class="value">Rp {{number_format($order->confirmation->amount,2,',','.')}}</div>
                                    </div>
                                    <div class="item">
                                        <div class="lebels">Ke Bank :</div>
                                        <div class="value">BANK {{strtoupper($order->confirmation->to)}}</div>
                                    </div>
                                    <div class="item">
                                        <div class="lebels">Approved :</div>
                                        <div class="value">
                                            <span class="label {{$approved[$order->confirmation->approved]['label']}}">{{$approved[$order->confirmation->approved]['name']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
@stop