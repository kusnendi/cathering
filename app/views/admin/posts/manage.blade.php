@extends('layout.admin')

@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">{{$pageTitle}}</span>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-12" style="margin-bottom: 5px !important">
                <div class="btn-group btn-group-sm" role="group" aria-label="Default button group">
                    <a href="{{URL::to('admin/pages/create')}}" class="btn btn-default">Add New</a>
                    <a href="{{URL::to('admin/pages')}}" class="btn btn-default">All</a>
                    <button type="button" class="btn btn-default">Published</button>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 hidden-xs" style="margin-bottom: 5px !important">
                <div class="pull-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <input class="form-control input-sm" type="text" name="s" value="{{(Input::has('s')) ? Input::get('s') : ''}}" placeholder="Keywords" />
                            <button type="submit" class="btn btn-default btn-sm">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body no-padding">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr class="bg-primary">
                                <th width="35%">Title</th>
                                <th width="15%">Section</th>
                                <th width="10%">Status</th>
                                <th width="15%">Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$pages->count())
                                <tr>
                                    <td colspan="4" class="text-danger">Nothing found in here...</td>
                                </tr>
                            @else
                                @foreach($pages as $page)
                                    <tr>
                                        <td>
                                            <a href="{{URL::to('admin/pages/edit/'.$page->id)}}">{{$page->title}}</a>
                                            <ul class="list-inline" style="font-size: 11px">
                                                <li><a href="{{URL::to('admin/pages/edit/'.$page->id)}}">Edit</a></li>
                                            </ul>
                                        </td>
                                        <td><span class="label label-warning">{{ucwords($page->section)}}</span></td>
                                        <td>{{($page->status) ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Disable</span>"}}</td>
                                        <td>{{$page->created_at}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
