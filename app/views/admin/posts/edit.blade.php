@extends('layout.admin')

@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">{{$pageTitle}}</span>
        </div>
        <div class="row">
            <form method="post" action="{{URL::action('PageController@postUpdate', $page->id)}}">
                <div class="col-sm-9">
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" value="{{$page->title}}" placeholder="Type Post Title Here" style="font-size: 18px" />
                    </div>
                    <div class="form-group">
                        <textarea class="wysiwyg form-control" name="body" rows="20">{{$page->content}}</textarea>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Page Section</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {{Form::select('section', ['aktivasi' => 'aktivasi','konfirmasi' => 'konfirmasi','registrasi' => 'registrasi'], $page->section, ['class' => 'form-control'] )}}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Update</button>
                        <div class="pull-right">
                            <a href="{{URL::to('admin/pages')}}" class="btn btn-default">Batal</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop