@extends('layout.admin')

@section('content')
    <div class="side-body">
        <div class="page-title">
            <span class="title">{{$pageTitle}}</span>
        </div>
        <div class="row">
            <form method="post" action="{{URL::action('PageController@postCreate')}}">
            <div class="col-sm-9">
                <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Type Post Title Here" style="font-size: 18px" />
                </div>
                <div class="form-group">
                    <textarea class="wysiwyg form-control" name="body" rows="20"></textarea>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Page Section</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {{Form::select('section', ['aktivasi' => 'aktivasi','konfirmasi' => 'konfirmasi','registrasi' => 'registrasi'], 'registrasi', ['class' => 'form-control'] )}}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-info">Publish</button>
                    <div class="pull-right">
                        <button type="reset" class="btn btn-default">Batal</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
@stop