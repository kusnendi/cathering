@extends('layouts.admin')

@section('content')
    <div class="side-body">
        <div class="page-main" style="padding-top: 30px">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="glyphicon glyphicon-credit-card"></i> {{$pageTitle}}</div>
                        <div class="panel-body">
                            <div id="ajax-cm" class=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop