<table class="table table-hover">
    <thead>
    <tr>
        <th width="20%">Code</th>
        <th width="10%">Type</th>
        <th width="15%">Published</th>
        <th width="10%">Amount</th>
        <th width="10%">Current Amount</th>
        <th width="15%"></th>
        <th width="10%" class="text-left">Status</th>
    </tr>
    </thead>

    <tbody>
    @if($vouchers->count())
        @foreach($vouchers as $voucher)
            <tr>
                <td style="letter-spacing: 1px">{{$voucher->code}}</td>
                <td>{{ucwords($voucher->template)}}</td>
                <td>{{date('d/m/Y, H:i', strtotime($voucher->created_at))}}</td>
                <td>Rp {{number_format($voucher->amount,2,',','.')}}</td>
                <td>Rp {{($voucher->logs) ? number_format($voucher->logs[0]->debit,2,',','.') : number_format($voucher->amount,2,',','.')}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            @foreach($act_options as $action)
                                @if($action['name'] == 'hapus')
                                    <li><a href="{{url($action['action'])}}" data-id="{{$voucher->id}}">{{ucfirst($action['name'])}}</a></li>
                                @else
                                    <li><a href="{{url($action['action'],$voucher->id)}}">{{ucfirst($action['name'])}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td class="text-left">{{$status[$voucher->status]}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div>
    {{$vouchers->appends(array_except(Input::query(), 'page'))->links()}}
</div>