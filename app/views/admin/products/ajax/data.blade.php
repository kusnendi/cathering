<table class="table table-hover">
    <thead>
    <tr>
        <th width="5%"></th>
        <th width="25%">Nama</th>
        <th width="20%">Price</th>
        <th width="15%">Point</th>
        <th width="10%">Category</th>
        <th width="10%">View</th>
        <th width="5%"></th>
        <th width="10%" class="text-right">Status</th>
    </tr>
    </thead>

    <tbody>
    @if($products->count())
        @foreach($products as $product)
            <tr>
                <td class="text-center">
                    <div style="width: 50px; text-align: center" class="text-center">
                        <img class="img-responsive text-center" src="{{asset($product->image)}}" />
                    </div>
                </td>
                <td>{{$product->product}}</td>
                <td>{{number_format($product->price,2,',','.')}}</td>
                <td>{{$product->point}}</td>
                <td>{{$product->category->category}}</td>
                <td>0</td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            @foreach($act_options as $action)
                                @if($action['name'] == 'hapus')
                                    <li><a href="{{url($action['action'])}}" data-id="{{$product->id}}">{{ucfirst($action['name'])}}</a></li>
                                @elseif($action['name'] != 'lihat')
                                    <li><a href="{{url($action['action'],$product->id)}}">{{ucfirst($action['name'])}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="dropdown" data-toggle="dropdown" style="cursor:pointer;">{{ucfirst($status[$product->status])}} <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            @foreach($status as $key => $value)
                                @if($key == $product->status)
                                    <li class="dropdown-header">{{ucfirst($value)}}</li>
                                @else
                                    <li><a href="#">{{ucfirst($value)}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<div>
    {{$products->appends(array_except(Input::query(),'page'))->links()}}
</div>