@extends('layouts.admin')

@section('content')
    <div class="side-body">
        <div class="page-main" style="padding-top: 30px">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-users"></i> {{$pageTitle}}</div>
                        <div class="panel-body">
                            <div class="">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th width="10%">IDs</th>
                                        <th width="15%">Nama Lengkap</th>
                                        <th width="20%">Email</th>
                                        <th width="15%">Registered</th>
                                        <th width="15%">Type</th>
                                        <th width="10%"></th>
                                        <th width="5%" class="text-right">Status</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if($users->count())
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{sprintf('%06s', $user->id)}}</td>
                                                <td>{{$user->profile->fullname}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{date('d/m/Y, H:i', strtotime($user->created_at))}}</td>
                                                <td>{{$user_type[$user->type]}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            @foreach($act_options as $action)
                                                                @if($action['name'] == 'hapus')
                                                                    <li><a href="{{url($action['action'])}}" data-id="{{$user->id}}">{{ucfirst($action['name'])}}</a></li>
                                                                @else
                                                                    <li><a href="{{url($action['action'],$user->id)}}">{{ucfirst($action['name'])}}</a></li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <div class="btn-group">
                                                        <a class="dropdown" data-toggle="dropdown" style="cursor:pointer;">{{$status[$user->status]}} <span class="caret"></span></a>
                                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                            @foreach($status as $key => $value)
                                                                @if($key == $user->status)
                                                                    <li class="dropdown-header">{{$value}}</li>
                                                                @elseif($key != '3' && $key != '0')
                                                                    <li><a href="#">{{$value}}</a></li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop