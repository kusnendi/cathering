@extends('layouts.admin')

@section('content')
    <div class="side-body padding-top">
        <div class="row no-margin-bottom">
            <div class="col-sm-12 col-lg-12 no-margin">
                <div class="panel panel-default">
                    <div class="panel-body quick-shortcut">
                        <div class="row no-margin-bottom">
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="#" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/categories_icon.png')}}" />
                                    <h5>Categories</h5>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="{{url('admin/products')}}" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/products_icon.png')}}" />
                                    <h5>Products</h5>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="{{url('admin/users?type=administrator')}}" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/customers_icon.png')}}" />
                                    <h5>Pelanggan</h5>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="{{url('admin/orders')}}" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/orders_icon.png')}}" />
                                    <h5>Pesanan</h5>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="#" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/icon_layouts.png')}}" />
                                    <h5>Mailbox</h5>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 shortcut text-center" style="margin: 0px">
                                <a href="#" class="" title="">
                                    <img class="img-circle" src="{{asset('resources/icons/icon_manage3.png')}}" />
                                    <h5>Testimonial</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="{{URL::to('admin/accounts?status=new')}}">
                    <div class="card summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-calendar-check-o fa-4x"></i>
                            <div class="content">
                                <div class="title">{{$users['new']}}</div>
                                <div class="sub-title">Catering</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="{{URL::to('admin/accounts?status=confirmations')}}">
                    <div class="card summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-cutlery fa-4x"></i>
                            <div class="content">
                                <div class="title">{{$users['request']}}</div>
                                <div class="sub-title">Ready to Eat</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="{{URL::to('admin/accounts?status=activated')}}">
                    <div class="card summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-product-hunt fa-4x"></i>
                            <div class="content">
                                <div class="title">{{$users['active']}}</div>
                                <div class="sub-title">Buy Points</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="{{URL::to('admin/accounts?status=expired')}}">
                    <div class="card summary-inline">
                        <div class="card-body">
                            <i class="icon fa fa-shopping-cart fa-4x"></i>
                            <div class="content">
                                <div class="title">{{$users['expired']}}</div>
                                <div class="sub-title">Live Carts</div>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row  no-margin-bottom">
            <div class="col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-dashboard"></i> Overview</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td width="60%">Total Sales :</td>
                                    <td class="text-right">Rp 1.500.00,00</td>
                                </tr>
                                <tr>
                                    <td width="60%">Total Sales This Year :</td>
                                    <td class="text-right">Rp 10.250.000,00</td>
                                </tr>
                                <tr>
                                    <td width="60%">Total Orders :</td>
                                    <td class="text-right">5</td>
                                </tr>
                                <tr>
                                    <td width="60%">Total Customers :</td>
                                    <td class="text-right">1</td>
                                </tr>
                                <tr>
                                    <td width="60%">Total Products :</td>
                                    <td class="text-right">15</td>
                                </tr>
                                <tr>
                                    <td width="60%">Total Testimonial :</td>
                                    <td class="text-right">0</td>
                                </tr>
                                <tr>
                                    <td width="60%">Testimoni waiting approval :</td>
                                    <td class="text-right">0</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-bar-chart-o"></i> Statistics</div>
                    <div class="panel-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop