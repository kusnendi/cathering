<script type="text/javascript" src="{{asset('assets/admin/lib/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/jquery.matchHeight-min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/ace/ace.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/ace/mode-html.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/lib/js/ace/theme-github.js')}}"></script>

<!-- Javascript -->
<script type="text/javascript" src="{{asset('assets/admin/js/app.js')}}"></script>
{{--
<script type="text/javascript" src="{{asset('assets/admin/js/index.js')}}"></script>--}}
