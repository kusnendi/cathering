<div class="side-menu">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <div class="icon fa fa-cutlery"></div>
                    <div class="title">MEALTIME</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li class="{{(Request::segment(2) == 'dashboard') ? 'active' : '' }}">
                    <a href="{{URL::to('admin/dashboard')}}">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="dropdown {{(Request::segment(2) == 'orders') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#dropdown-orders">
                        <span class="icon fa fa-shopping-cart"></span><span class="title">Orders</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-orders" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/orders')}}">Catering</a></li>
                                <li><a href="{{URL::to('admin/orders')}}">Ready to Eat</a></li>
                                <li><a href="{{URL::to('admin/orders/packages')}}">Packages</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown {{(Request::segment(2) == 'products') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#dropdown-products">
                        <span class="icon fa fa-cubes"></span><span class="title">Catalog</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-products" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/categories')}}">Categories</a></li>
                                <li><a href="{{URL::to('admin/products')}}">All Products</a></li>
                                <li><a href="{{URL::to('admin/products/recommended')}}">Chef Recommended</a></li>
                                <li><a href="{{URL::to('admin/products/create')}}">Add New</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown {{(Request::segment(2) == 'users') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#dropdown-users">
                        <span class="icon fa fa-users"></span><span class="title">Users</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-users" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/users?type=administrator')}}">Administrator</a></li>
                                <li><a href="{{URL::to('admin/users?type=customer')}}">Customer</a></li>
                                <li><a href="{{URL::to('admin/users/create')}}">Add New</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown {{(Request::segment(2) == 'mailbox') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#dropdown-mailbox">
                        <span class="icon fa fa-envelope"></span><span class="title">Mailbox</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-mailbox" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/pages/create')}}">Inbox</a></li>
                                <li><a href="{{URL::to('admin/pages/create')}}">Trash</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
                    <a data-toggle="collapse" href="#dropdown-pages">
                        <span class="icon fa fa-line-chart"></span><span class="title">Marketing</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-pages" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/pages/create')}}">Live Carts</a></li>
                                <li><a href="{{URL::to('admin/pages/create')}}">News Letter</a></li>
                                <li><a href="{{URL::to('admin/vouchers')}}">Gift Certificates</a></li>
                                <li><a href="{{URL::to('admin/pages/create')}}">Banner</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
                    <a data-toggle="collapse" href="#dropdown-website">
                        <span class="icon fa fa-globe"></span><span class="title">Website</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-website" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/pages/create')}}">Posts</a></li>
                                <li><a href="{{URL::to('admin/pages')}}">Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <!-- Dropdown-->
                <li class="panel panel-default dropdown {{(Request::segment(2) == 'settings') ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#component-example">
                        <span class="icon fa fa-cogs"></span><span class="title">Settings</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="component-example" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{{URL::to('admin/settings')}}">General</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                {{--
                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#dropdown-form">
                        <span class="icon fa fa-file-text-o"></span><span class="title">User</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-form" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="#">Form UI Kits</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                --}}
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>