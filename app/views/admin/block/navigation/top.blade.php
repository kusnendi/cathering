<ul class="nav navbar-nav navbar-right">
    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
        <i class="fa fa-times icon"></i>
    </button>
    {{--<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
        <ul class="dropdown-menu animated fadeInDown">
            <li class="title">
                Notification <span class="badge pull-right">0</span>
            </li>
            <li class="message">
                No new notification
            </li>
        </ul>
    </li>
    <li class="dropdown danger">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> {{$notifications['all']}}</a>
        <ul class="dropdown-menu danger  animated fadeInDown">
            <li class="title">
                Notification <span class="badge pull-right">{{$notifications['all']}}</span>
            </li>
            <li>
                <ul class="list-group notifications">
                    <a href="{{URL::to('admin/accounts?status=new')}}">
                        <li class="list-group-item">
                            <span class="badge">{{$notifications['registers']}}</span> <i class="fa fa-users icon"></i> new registration
                        </li>
                    </a>
                    <a href="{{URL::to('admin/accounts?status=confirmations')}}">
                        <li class="list-group-item">
                            <span class="badge success">{{$notifications['confirms']}}</span> <i class="fa fa-check icon"></i> request confirmations
                        </li>
                    </a>
                </ul>
            </li>
        </ul>
    </li>--}}
    <li class="dropdown profile">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{Auth::user()->email}} <span class="caret"></span></a>
        <ul class="dropdown-menu animated fadeInDown">
            <li class="profile-img">
                <img src="{{asset('logo/mealtime.png')}}" class="img-responsive">
            </li>
            <li>
                <div class="profile-info">
                    <h4 class="username">{{Auth::user()->user_login}}</h4>
                    <p>{{Auth::user()->email}}</p>
                    <div class="btn-group margin-bottom-2x" role="group">
                        <a href="#" class="btn btn-default"><i class="fa fa-user"></i> Profile</a>
                        <a href="{{URL::to('logout')}}" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                </div>
            </li>
        </ul>
    </li>
</ul>