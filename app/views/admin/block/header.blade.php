<div class="navbar-header">
    <button type="button" class="navbar-expand-toggle">
        <i class="fa fa-bars icon"></i>
    </button>
    <ol class="breadcrumb navbar-breadcrumb">
        @if(count($breadcumbs))
            {{--*/ $i = count($breadcumbs); $n = 0; /*--}}
            @foreach($breadcumbs as $breadcumb)
                @if($n>=$i)
                    <li class="active">{{$breadcumb}}</li>
                @endif
                <li class="">{{$breadcumb}}</li>
            @endforeach
        @endif
    </ol>
    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
        <i class="fa fa-th icon"></i>
    </button>
</div>