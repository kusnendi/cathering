@extends('layout.admin')

@section('content')
<div class="side-body">
    <div class="page-title">
        <span class="title">General Setting</span>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <form method="post" action="{{URL::action('SettingController@postIndex')}}" class="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Site Title</label>
                    <input type="text" name="title" class="form-control" value="{{$identity->title}}" required="required" />
                </div>
                <div class="form-group">
                    <label>Tagline</label>
                    <input type="text" name="tagline" class="form-control" value="{{$identity->tagline}}" />
                </div>
                <div class="form-group">
                    <label>Keywords</label>
                    <input type="text" name="keyword" class="form-control" value="{{$identity->keyword}}" />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="{{$identity->email}}" required="required" />
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" rows="4">{{$identity->description}}</textarea>
                </div>
                <div class="form-group">
                    <label>Favicon</label>
                    <input type="file" name="favicon" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Logo</label>
                    <input type="file" name="logo" class="form-control" />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info">Save Changes</button>
                </div>
            </form>
        </div>
        <div class="col-xs-6">
        </div>
    </div>
</div>
@stop
