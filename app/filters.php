<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	/* Store Session ID */
    If (!Session::has('sid')) Session::put('sid', Session::getId());

    /* Cart items */
    $carts = Cart::whereSessionId(Session::get('sid'))->get();
    View::share('carts', $carts);

    /* Initial Area */
    if (!Session::has('area'))
    {
        if (Auth::check())
        {
            Session::put('area', 'customer');
        }
        else
        {
            Session::put('area', 'guest');
        }
    }
    else
    {
        if (!Auth::check())
        {
            (Session::get('area') == 'customer') ? Session::put('area', 'guest') : false;
        }
        else
        {
            Session::put('area', 'customer');
        }

        if (Session::get('area') == 'customer')
        {
            $new_order = Order::whereUserId(Auth::id())
                ->whereStatus('O')
                ->count();

            View::share('n_order', $new_order);
        }
    }

    /**
     * General var @ Admin
     * initial content
     */
    if (Auth::check() && Auth::user()->type == 0)
    {
        /*$user_type = json_decode(file_get_contents(storage_path('extra/user_type.json')));
        $u_type = [];
        foreach ($user_type as $key => $value)
        {
            $u_type[$key] = $value;
        }*/

        /**
         * prepare array section statuses
         * return array common
         */
        switch(Request::segment(2))
        {
            case 'orders':
                /* Order Status */
                View::share('status', Order::status());
                View::share('approved', OrderConfirmation::approved());
                break;
            case 'users' :
                /* User Types */
                View::share('user_type', User::types());
                /* User Status */
                View::share('status', User::status());
                break;
            case 'vouchers' :
                /* Vocuher Status */
                View::share('status', GiftCert::status());
                break;
            case 'products' :
                View::share('status', Product::status());
                break;
            default :
                break;
        }

        /**
         * prepare section common action options
         * return array
         */
        View::share('act_options', General::actions(Request::segment(2)));
    }

});


App::after(function($request, $response)
{

});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});

/*
 * Administration Filter
 */
Route::filter('admin', function()
{
    if (Auth::check())
    {
        if (Auth::user()->type == 9)
        {
            return Redirect::to('accounts');
        }
    }
    else
    {
        return Redirect::guest('login');
    }
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check())
    {
        switch (Auth::user()->type)
        {
            case 9 :
                $redirect = Redirect::to('accounts');
                break;
            default;
                $redirect = Redirect::to('admin/dashboard');
                break;
        }

        return $redirect;
    }

});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
