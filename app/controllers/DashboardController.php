<?php

class DashboardController extends \BaseController {

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

        $breadcumbs = [
            'Dashboard'
        ];

		//Check user type
        $users = [
            'new'       => 0,
            'active'    => 0,
            'request'   => 0,
            'expired'   => 0
        ];

        $notifications = [
            'registers' => 0,
            'confirms'  => 0,
            'all'       => 0
        ];

        return View::make('admin.dashboard.index', compact('users','notifications','breadcumbs'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


}
