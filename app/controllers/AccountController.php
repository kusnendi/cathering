<?php

class AccountController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$profil = UserProfile::whereUserId(Auth::user()->id)->first();


        $arrays = compact('profil');
        return View::make('web.accounts.profil', $arrays);
	}

    public function postIndex()
    {
        $rules  = [
            'fullname'      => 'required',
            'phone'         => 'required',
            'address'       => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);
        $messages  = $validator->messages();

        if($validator->passes())
        {
            $profil = UserProfile::whereUserId(Auth::user()->id)->first();

            $profil->fullname = Input::get('fullname');
            $profil->phone = Input::get('phone');
            $profil->address = Input::get('address');
            $profil->save();
        }

        return Redirect::back();
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getChangePassword()
	{
		return View::make('web.accounts.changePassword');
	}

    public function postChangePassword()
    {
        $rules = [
            'password'      => 'required',
            'new_password'  => 'required|same:confirm_password'
        ];

        $validation = Validator::make(Input::all(), $rules);
        $messages = $validation->messages();

        if ($validation->fails()) return Redirect::back();

        if (Hash::check(Input::get('password'), Auth::user()->password()))
        {
            $user = User::find(Auth::id());
            $user->password = Hash::make(Input::get('new_password'));
            $user->save();
        }

        return Redirect::to('accounts');
    }

}
