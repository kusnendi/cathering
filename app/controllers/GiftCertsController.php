<?php

class GiftCertsController extends \BaseController {


    /** Manage Vouchers */
    public function getIndex()
    {
        if (Auth::user()->type != 9)
        {
            $pageTitle = 'Voucher';
            $breadcumbs= [
                'Voucher',
                'Manage'
            ];

            $vouchers = GiftCert::with(array('logs' => function($query){
                $query->orderBy('id','desc');
            }))->paginate($this->per_page);

            /*if(Request::ajax())
            {
                return View::make('admin.vouchers.ajax.data', compact('vouchers'))->render();
            }*/

            $compact = compact('pageTitle','breadcumbs','vouchers');
            return View::make('admin.vouchers.manage', $compact);
        }
    }

    public function getAjax()
    {
        if(Request::ajax())
        {
            $vouchers = GiftCert::with(array('logs' => function($query){
                $query->orderBy('id','desc');
            }))->paginate($this->per_page);

            return View::make('admin.vouchers.ajax.data', compact('vouchers'))->render();
        }
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postVerify()
	{
		$rules = [
            'code'  => 'required|string'
        ];

        $validation = Validator::make(Input::all(), $rules);
        $messages = $validation->messages();

        if ($validation->fails())
        {
            return false;
        }

        $voucher = GiftCert::whereCode(Input::get('code'))
            ->whereStatus('A')
            ->first();

        $response = [
            'response' => false,
            'message'  => 'voucher invalid!'
        ];

        if ($voucher)
        {
            /* remember gift voucher until checkout done / cancel */
            //Session::forget('voucher');
            Session::put('voucher', $voucher->toArray());

            $debit = $voucher->amount;
            $cart  = Cart::Subtotal(Session::get('sid'));
            $status= true;
            if (!empty($voucher->order_ids))
            {
                $order_ids = explode(',', $voucher->order_ids);
                $voucher_history = GiftCertHistory::whereIn('order_id', $order_ids)
                    ->orderBy('id', 'desc')
                    ->first();

                if ($cart->subtotal >= $voucher_history->debit && $voucher_history->debit <> 0)
                {
                    //$debit = $voucher->amount;
                    $status = true;
                    $debit  = (int)$voucher_history->debit;
                    $credit = 0;
                    $total  = $cart->subtotal-$voucher_history->debit;
                    /*$response = [
                        'response'  => true,
                        'debit'     => number_format((int)$voucher_history->debit,0,',','.'),
                        'credit'    => 0,
                        'total'     => number_format($cart->subtotal-$voucher_history->debit,0,',','.')
                    ];*/
                }
                elseif ($voucher_history->debit == 0)
                {
                    if(Session::has('voucher')) Session::forget('voucher');

                    $response = [
                        'response'  => false,
                        'message'   => 'Invalid Voucher Code !'
                    ];
                }
                else
                {
                    $status = true;
                    $debit  = $cart->subtotal;
                    $credit = $voucher_history->debit - $debit;
                    $total  = 0;
                    /*$response = [
                        'response'  => true,
                        'debit'     => number_format((int)$debit,0,',','.'),
                        'credit'    => $voucher_history->debit - $debit,
                        'total'     => 0
                    ];*/
                }
            }
            else
            {

                if ($cart->subtotal >= $voucher->amount)
                {
                    //$debit = $voucher->amount;
                    $status   = true;
                    $credit   = 0;
                    $total    = $cart->subtotal-$debit;
                    /*$response = [
                        'response'  => true,
                        'debit'     => number_format((int)$debit,0,',','.'),
                        'credit'    => 0,
                        'total'     => number_format($cart->subtotal-$debit,0,',','.')
                    ];*/
                }
                else
                {
                    $status = true;
                    $debit  = $cart->subtotal;
                    $credit = $voucher->amount - $debit;
                    $total  = 0;
                    /*$response = [
                        'response'  => true,
                        'debit'     => number_format((int)$debit,0,',','.'),
                        'credit'    => $voucher->amount - $debit,
                        'total'     => 0
                    ];*/
                }
            }

            if ($status) {

                $response = [
                    'response'  => $status,
                    'debit'     => number_format($debit,0,',','.'),
                    'credit'    => number_format($credit,0,',','.'),
                    'total'     => number_format($total,0,',','.')
                ];

                Session::put('voucher.debit', $debit);
                Session::put('voucher.credit', $credit);
            }
        }

        return $response;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getGenerate($n=null)
	{
		/*$prefix = 'GF-';
        while ($n <= 10)
        {
            $str = str_random(12);
            $code= '';
            $code.= substr($str,0,4).'-';
            $code.= substr($str,4,4).'-';
            $code.= substr($str,8,4);
            $code = $prefix.$code;

            //echo strtoupper($str)."\n";
            echo strtoupper($code);
            echo "<br/>";
            $n++;
        }*/

        $voucher = Session::forget('voucher');

        /*$gc_logs = GiftCertHistory::find(1);
        $gc_logs->load('coupon');*/

        echo '<pre>';
        /*print_r($gc_logs->coupon->toArray());*/
        print_r($voucher);
	}


}
