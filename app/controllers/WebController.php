<?php

class WebController extends \BaseController {

	public function getIndex()
    {
        return View::make('web.index');
    }

    public function getGallery()
    {
        return View::make('web.gallery.gallery');
    }

    public function getCheckout()
    {
        $checkout_flag = 'default';
        //set session event
        Session::put('event', 'checkout');

        /*Session::forget('voucher');exit();*/

        $voucher= [];
        $carts  = Cart::where('session_id', Session::get('sid'))->get();
        $subtotal = Cart::Subtotal(Session::get('sid'));

        if (!$carts->count()) return Redirect::back();

        if (Session::has('voucher'))
        {
            // Voucher Validation
            $voucher = Session::get('voucher');
            $voucher = GiftCert::find($voucher['id']);
            $voucher = $voucher->toArray();

            if ($voucher['order_ids'])
            {
                $voucher_logs = GiftCertHistory::whereIn('order_id', explode(',', $voucher['order_ids']))
                    ->orderBy('id','desc')
                    ->first();
                $voucher_logs->load('coupon');
                //$voucher = $voucher_logs->coupon->toArray();

                if($subtotal->subtotal >= $voucher_logs->debit)
                {
                    $voucher['debit'] = $voucher_logs->debit;
                    $voucher['credit']= 0;
                }
                else
                {
                    $voucher['debit'] = $subtotal->subtotal;
                    $voucher['credit']= $voucher['amount']-$subtotal->subtotal;
                }
            }
            else
            {
                if ($subtotal->subtotal >= $voucher['amount'])
                {
                    $voucher['debit'] = $voucher['amount'];
                    $voucher['credit']= 0;
                }
                else
                {
                    $voucher['debit'] = $subtotal->subtotal;
                    $voucher['credit']= $voucher['amount']-$subtotal->subtotal;
                }
            }

            Session::put('voucher', $voucher);
        }

        $total = (isset($voucher['debit'])) ? $subtotal->subtotal-$voucher['debit'] : $subtotal->subtotal;

        if ($carts->count() == 1 && $carts[0]->section_id == 2) {
            $checkout_flag = 'points';
        }

        $array = compact('checkout_flag','carts','subtotal','voucher','total');
        return View::make('web.checkout', $array);
    }

    public function postCheckout()
    {

        $subtotal = Cart::Subtotal(Session::get('sid'));
        $shipping_cost = Input::has('shipping_cost') ? Input::get('shipping_cost') : 0;
        $ppn = 0;

        //Calculate Total Transaction
        $total   = $subtotal->subtotal+$shipping_cost+$ppn;
        $voucher = [];
        $vc_log  = [];
        if (Session::has('voucher'))
        {
            $voucher = Session::get('voucher');
            $total   = ($subtotal->subtotal+$shipping_cost+$ppn)-$voucher['debit'];
            //Store voucher logs
            $vc_log = [
                'gift_cert_id'  => $voucher['id'],
                'user_id'       => Auth::id(),
                'amount'        => $voucher['debit']+$voucher['credit'],
                'debit'         => $voucher['credit'],
                'products'      => $voucher['products'],
            ];
        }

        $order = new Order();
        $order->user_id = Auth::id();
        $order->fullname = User::fullname(Auth::id());
        $order->shipping_cost = $shipping_cost;
        $order->subtotal = $subtotal->subtotal;
        $order->total = $total;
        $order->notes = Input::get('notes');
        $order->payment_id = (Input::has('payment_method')) ? Input::get('payment_method') : 0;
        $order->status = ($total == 0) ? 'P' : 'O';

        if($order->save())
        {
            //Store Item at Order Details
            $carts = Cart::where('session_id', Session::get('sid'))->get();
            $order_details = [];
            foreach ($carts as $cart)
            {
                $order_details[] = [
                    'order_id'  => $order->id,
                    'product_id'=> $cart->product_id,
                    'amount'    => $cart->amount,
                    'price'     => $cart->price,
                    'extra'     => $cart->extra,
                    'created_at'=> new DateTime(),
                ];
            }

            DB::table('order_details')->insert($order_details);

            //Log Gift Cert
            if (count($vc_log))
            {
                //update gift certs
                $voc = GiftCert::find($voucher['id']);
                $voc->order_ids = ($voc->order_ids) ? $voc->order_ids.','.$order->id : $order->id;
                $voc->status = ($vc_log['debit'] <> 0) ? 'A' : 'U';
                $voc->save();

                $vc_log['order_id'] = $order->id;
                GiftCertHistory::create($vc_log);
            }

            Session::forget('voucher');
            //Clear Carts
            Cart::where('session_id', Session::get('sid'))->delete();

            return Redirect::to('add-point');
        }

        return Redirect::back();

    }

    public function getAddPoint()
    {
        return View::make('web.packet');
    }

    public function getLogin()
    {
        $auth   = Auth::check();

        if ($auth)
        {
            return Redirect::to('accounts');
        }

        return View::make('web.auth.login');
    }

    public function postLogin()
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);
        $messages  = $validator->messages();

        $return = '';

        if($validator->fails())
        {
            $return = Redirect::back()
                ->withInput(Input::except('password'));
        }
        else
        {
            $credentials = [
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
                'status'    => 1
            ];

            //attempt to do login
            if(Auth::attempt($credentials))
            {
                if(Auth::user()->type != 9)
                {
                    return Redirect::to('admin/dashboard');
                }

                $route  = (Session::has('event')) ? Session::pull('event') : 'accounts';
                $return = Redirect::intended($route);

                $user = User::find(Auth::id());
                Event::fire('auth.onLogin', array($user));
            }
            else
            {
                $return = Redirect::to('login')
                    ->withInput(Input::except('password'));
            }
        }

        return $return;
    }

    public function getRegister()
    {
        return View::make('web.auth.register');
    }

    public function postRegister()
    {
        $rules = [
            'name'      => 'required|min:6',
            'email'     => 'required|email',
            'password'  => 'required|same:confirm_password'
        ];

        //Input Validation
        $validator = Validator::make(Input::all(), $rules);
        $messages  = $validator->messages();

        if($validator->fails())
        {
            return Redirect::to('register')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
        else
        {

            $data_user = [
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
                'key'       => Str::random(64),
                'type'      => 9,
                'status'    => 0
            ];

            $data_profile = [
                'fullname'  => Input::get('name')
            ];

            //Create User Account
            $user = User::create($data_user);
            $data_profile['user_id'] = $user->id;

            //Create User Profile
            $profile = UserProfile::create($data_profile);

            $_data = [
                'fullname' => $profile->fullname,
                'token'    => $user->key
            ];

            Mail::queue('emails.auth.confirmation', $_data, function($message) use($user)
            {
                $message->to($user->email)->subject('Welcome!');
            });

            return Redirect::to('register?action=success')
                ->with('profile', $profile->toArray())
                ->with('user', $user->toArray());
        }
    }

    public function anyActivation()
    {
        if(Input::has('token'))
        {
            $user = User::whereKey(Input::get('token'))->first();

            if($user)
            {
                //Activate User
                $user->status = 1;
                $user->save();

                Auth::loginUsingId($user->id);
                return Redirect::to('accounts');
            }
            else
            {
                return Redirect::to('register');
            }
        }
    }

    /*
     * Logout Process
     */
    public function getLogout()
    {

        $user   = Auth::check();
        if ($user)
        {
            /* Fire Logout Event */
            Event::fire('auth.onLogout', $user);
            //redirect to user login
        }

        return Redirect::to('login');
    }

    public function getMenu()
    {
        return View::make('web.menu');
    }

    public function getAboutUs()
    {
        return View::make('web.pages.about');
    }

    public function getContactUs()
    {
        return View::make('web.pages.contactUs');
    }

    public function getSession()
    {

        /*if (!Session::has('carts'))
        {
            $carts = [
                'items' => array(
                    [
                        'product_id'    =>  12122,
                        'product'       =>  'ABC'
                    ],
                    [
                        'product_id'    =>  12123,
                        'product'       =>  'ABD'
                    ]
                ),
                'subtotal'  =>  120000
            ];

            Session::put('carts', $carts);
        }
        else {
            /*$item = [
                'product_id'    => rand(11111,99999),
                'product'       => str_random(8)
            ];

            Session::push('carts.items', $item);

            $item = ['product' => 'inc234'];

            Session::push('carts.items.3', $item);
        }*/
        echo '<pre>';
        print_r(Session::get('carts'));

        /*foreach (Session::get('carts.items') as $item)
        {
            echo $item['product'] . "\n";
        }*/
    }

}
