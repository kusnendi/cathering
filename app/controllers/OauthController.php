<?php

class OauthController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getAuth()
	{
		try {
            Hybrid_Endpoint::process();
        } catch (Exception $e)
        {
            return Redirect::to('oauth/fb-info');
        }

        return;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getFbInfo()
	{
		try {
            $oautuh = new Hybrid_Auth(app_path('config/hybridauth.php'));
            $provider = $oautuh->authenticate('Facebook');
            //$profile  = $provider->getUserProfile();

            if($provider)
            {
                $profile = $provider->getUserProfile();

                echo '<pre>';
                print_r($profile);
            }
        } catch (Exception $e)
        {
            echo $e->getMessage();
        }
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getLink($action=null)
	{
		if (!is_null($action) && $action == 'auth')
        {
            try {
                Hybrid_Endpoint::process();
            }
            catch (Exception $e)
            {
                return Redirect::to('oauth/link');
            }

            return;
        }

        try {
            $hybrid = new Hybrid_Auth(app_path('config/hybridauth.php'));
            $provider = $hybrid->authenticate('LinkedIn');

            /*echo '<pre>';
            print_r($provider->getUserProfile());*/

            if($provider)
            {
                $profile = $provider->getUserProfile();

                echo $profile->identifier;
            }

            /*$provider->logout();*/
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
        }


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
