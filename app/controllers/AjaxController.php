<?php

class AjaxController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function anyAddToLunchBox()
	{
		$item = Input::get('item');
		$sid  = Session::get('sid');
		$product = Product::find($item);

		$cart = new Cart();
		$cart->section_id = $product->section_id;
		$cart->category_id= $product->category_id;
		$cart->product_id = $product->id;
		$cart->amount	  = 1;
		$cart->price 	  = $product->price;
		$cart->extra      = serialize(['product' => $product->product, 'point' => $product->point]);
		$cart->session_id = $sid;
		$cart->type       = 'catering';

		if($cart->save())
		{
			return Response::json($product, 200);
		}

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
