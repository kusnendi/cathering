<?php

class OrderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $breadcumbs = [
            'Orders',
            'Manage'
        ];

        if (Auth::user()->type == 0)
        {
            $pageTitle = 'Orders';

            $orders = Order::with('user.profile')
                ->orderBy($this->sort_by, $this->sort_order)
                ->paginate($this->per_page);

            /*if(Request::ajax())
            {
                //$orders->load('user');
                return Response::json(View::make('admin.orders.ajax.data', compact('orders'))->render());
            }*/

            $compact = compact('pageTitle','breadcumbs','orders');
            return View::make('admin.orders.manage', $compact);
        }

        return;
	}

    public function getAjax()
    {
        $orders = Order::with('user.profile')->paginate($this->per_page);

        //$orders->load('user');
        return View::make('admin.orders.ajax.data')
            ->with('orders', $orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getView($id)
    {

        if (Auth::user()->type != 9)
        {
            $pageTitle = 'View Order';
            $breadcumbs= [
                'Orders',
                'View'
            ];

            $order = Order::find($id);
            $order->load('user.profile');
            $order->load('confirmation');
            $order->load('details');
            $order->load('coupons.coupon');

            $compact = compact('pageTitle','breadcumbs','order');
            return View::make('admin.orders.view', $compact);
        }
        else
        {
            $order = Order::where('user_id', Auth::id())->find($id);
            $order->load('details');
            $order->load('coupons.coupon');

            $compact = compact('order');
            return View::make('web.orders.view', $compact);
        }

    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getPaymentConfirmation()
	{
		$orders = Order::whereUserId(Auth::id())
            ->where('status','O')
            ->lists('id','id');

        $compact= compact('orders');
        return View::make('web.orders.payments.confirmation', $compact);
	}

    public function postPaymentConfirmation()
    {
        $rules = [
            'order_id'      => 'required|integer',
            'sender_name'   => 'required',
            'date_transfer' => 'required',
            'amount'        => 'required',
            'to'            => 'required'
        ];

        $validation = Validator::make(Input::all(), $rules);
        $messages = $validation->messages();
        if ($validation->fails())
        {
            return Redirect::back()
                ->withInput(Input::all());
        }

        $data = [
            'order_id'      => Input::get('order_id'),
            'date'          => strtotime(Input::get('date_transfer')),
            'sender'        => Input::get('sender_name'),
            'amount'        => Input::get('amount'),
            'to'            => Input::get('to'),
            'approved'      => 0
        ];

        $confirmation = OrderConfirmation::create($data);

        $return = Redirect::back()->withInput(Input::all());
        if ($confirmation)
        {
            $return = Redirect::to('orders/history');

            //update order status to confirm payment
            $order = Order::find(Input::get('order_id'));
            $order->status = 'K';
            $order->save();
        }

        return $return;
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getRating()
	{
        return View::make('web.orders.rating');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getHistory()
	{

        $orders = Order::whereUserId(Auth::id())
            ->orderBy('id','desc')
            ->take(10)
            ->get();

        return View::make('web.orders.history', compact('orders'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getInvoice($id)
	{
		$pageTitle = strtoupper("Invoice #") . sprintf('%06s', $id);

        $order = Order::with('user.profile')->find($id);
        $order->load('details');
        $order->load('coupons.coupon');

        return View::make('emails.orders.invoice', compact('order','pageTitle'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getSerialize()
	{
		$a = 'a:3:{s:3:"set";s:3:"all";s:9:"set_value";s:1:"0";s:10:"conditions";a:2:{i:1;a:3:{s:8:"operator";s:2:"in";s:9:"condition";s:10:"categories";s:5:"value";s:3:"168";}i:2;a:3:{s:3:"set";s:3:"all";s:9:"set_value";s:1:"1";s:10:"conditions";a:1:{i:1;a:3:{s:8:"operator";s:2:"in";s:9:"condition";s:5:"users";s:5:"value";s:2:"11";}}}}}';
        $b = 'a:1:{i:1;a:3:{s:5:"bonus";s:16:"product_discount";s:14:"discount_bonus";s:13:"by_percentage";s:14:"discount_value";s:2:"40";}}';
        $p = 'a:2:{i:16;a:1:{s:14:"total_discount";d:388.80000000000001136868377216160297393798828125;}i:15;a:1:{s:7:"bonuses";a:1:{i:0;a:3:{s:5:"bonus";s:13:"free_shipping";s:5:"value";s:1:"1";s:12:"promotion_id";s:2:"15";}}}}';

        echo '<pre>';
        print_r(unserialize($a));
        print_r(unserialize($b));
        print_r(unserialize($p));

        echo strtoupper(str_random(15));
	}

    public function getPcre()
    {
        $code = 'GF-CJYZ-ZPOG-78AG';
        $hash = substr($code,0,3);
        $hash.= preg_replace('([A-L])', '*' ,substr($code,3,4));
        $hash.= preg_replace('([Q-Z])', '*' ,substr($code,7,5));
        $hash.= preg_replace('([A-X])', '*' ,substr($code,12,5));
        echo $hash;
    }

    public function getJson()
    {

        print_r(General::status());

    }


}
