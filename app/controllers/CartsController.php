<?php

class CartsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function postAdd()
	{
		$quantity = 1;
        $userId = 0;
        $session_id = (Session::has('sid')) ? Session::get('sid') : Session::getId();
        $user_type = 'U';
        $response = [];

        //Check Auth User
        if (Auth::check())
        {
            $userId = Auth::id();
            $user_type = 'R';
        }

        $rules = [
            'product_id'    => 'required|integer',
            'amount'        => 'integer'
        ];

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails())
        {
            //
        }
        else
        {
            /** @var Check item on carts $product */
            $cart = Cart::where('session_id', Session::get('sid'))
                ->where('product_id', Input::get('product_id'))
                ->first();

            if ($cart)
            {
                $cart->amount = $cart->amount+Input::get('amount');
                $cart->save();
            }
            else {

                $product = Product::find(Input::get('product_id'));

                if ($product)
                {
                    $cart = new Cart();
                    $cart->user_id      = $userId;
                    $cart->user_type    = $user_type;
                    $cart->product_id   = $product->id;
                    $cart->section_id   = $product->section_id;
                    $cart->category_id  = $product->category_id;
                    $cart->amount       = Input::get('amount');
                    $cart->price        = $product->price;
                    $cart->session_id   = $session_id;
                    $cart->extra        = serialize(['product' => $product->product, 'display_price' => $product->display_price]);
                    $cart->save();
                }
            }

            //Calculate & Update Subtotal, total
            $subtotal = Cart::Subtotal(Session::get('sid'));
            (Session::has('voucher')) ? $voucher = Session::get('voucher') : $voucher=[];
            (isset($voucher['debit'])) ? $debit = $voucher['debit'] : $debit = 0;

            $total = $subtotal->subtotal-$debit;

            $item = [
                'product_id'    => $cart->product_id,
                'price'         => $cart->price,
                'amount'        => $cart->amount,
                'section_id'    => $cart->section_id,
                'category_id'   => $cart->category_id,
                'extra'         => unserialize($cart->extra)
            ];

            /*if (Session::has('carts'))
            {
                $item_id = Session::get('carts.items.' . $item['product_id']);
                if (isset($item_id))
                {
                    $item_s = Session::get('carts.items.' . $item['product_id']);
                    $item_s['amount'] = $item['amount'];

                    Session::put('carts.items.' . $item_id, $item_s);
                }
                else
                {
                    Session::put('carts.items.' . $item['product_id'], $item);
                }
            }
            else
            {
                $carts  = [
                    'items'     => [
                        $item['product_id'] => $item
                    ],
                    'subtotal'  => number_format($subtotal->subtotal,0,',','.')
                ];

                Session::put('carts', $carts);
            }*/

            $data = [
                'item'  => $item,
                'subtotal' => number_format($subtotal->subtotal,0,',','.'),
                'total'    => number_format($total,0,',','.')
            ];

            $response = Response::json($data, 200);
        }

        return $response;
	}

}
