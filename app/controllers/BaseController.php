<?php

class BaseController extends Controller {

    /* item per page */
    protected $per_page = 2;
    protected $sort_by = 'id';
    protected $sort_order = 'desc';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    /**
     * @param array $parameters
     * @return mixed
     * Throwing Missing Action
     */
    public function missingMethod($parameters = array())
    {
        if(Auth::check())
        {
            if(Auth::user()->type != 9)
            {
                return Redirect::back();
            }
            else
            {
                return Redirect::to('accounts');
            }
        }
        return Redirect::to('/');
    }

}
