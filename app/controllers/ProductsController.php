<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $pageTitle = "Products";
        $breadcumbs= [
            'Products',
            'Manage'
        ];

        $products = Product::where('section_id','1')
            ->paginate($this->per_page);

        $compact = compact('pageTitle','breadcumbs','products');
        return View::make('admin.products.manage', $compact);
	}

    public function getAjax()
    {
        $products = Product::where('section_id','1')
            ->paginate($this->per_page);

        //$orders->load('user');
        return View::make('admin.products.ajax.data')
            ->with('products', $products);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
