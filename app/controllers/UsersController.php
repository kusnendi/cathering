<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/28/2016
 * Time: 10:15 AM
 */

class UsersController extends BaseController {

    /**
     * Manage Users
     */
    public function getIndex()
    {
        $pageTitle = ucfirst(Input::get('type'));
        $breadcumbs = [
            'Users',
            ucfirst(Input::get('type'))
        ];

        $user_type = Input::get('type');
        $type = [
            'administrator' => 0,
            'customer'      => 9
        ];
        $users = User::with('profile')->where('type', $type[$user_type])->paginate(15);
        //$users->load('profile');

        $compact = compact('pageTitle','breadcumbs','users');
        return View::make('admin.users.manage', $compact);
    }
}