<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/23/2016
 * Time: 2:29 PM
 */

class CategoriesTableSeeder extends \Seeder {
    /*
     * Run Seeder
     */
    public function run()
    {
        /*//delete records from table users
        DB::table('categories')->delete();

        Category::create([
            'category'      => 'Catering',
            'seo_name'      => 'catering',
            'page_title'    => 'Catering - Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'category'      => 'Ready to Eat',
            'seo_name'      => 'ready-to-eat',
            'page_title'    => 'Ready to Eat - Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 1,
            'category'      => 'Carbo',
            'seo_name'      => 'carbo',
            'page_title'    => 'Carbo - Catering | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 1,
            'category'      => 'Meat',
            'seo_name'      => 'meat',
            'page_title'    => 'Meat - Catering | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 1,
            'category'      => 'Veggie',
            'seo_name'      => 'veggie',
            'page_title'    => 'Veggie - Catering | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 1,
            'category'      => 'Sides',
            'seo_name'      => 'sides',
            'page_title'    => 'Sides - Catering | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 1,
            'category'      => 'Dessert',
            'seo_name'      => 'dessert',
            'page_title'    => 'Dessert - Catering | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 2,
            'category'      => 'Bento',
            'seo_name'      => 'bento',
            'page_title'    => 'Bento - Ready to Eat | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 2,
            'category'      => 'Drinks',
            'seo_name'      => 'drinks',
            'page_title'    => 'Drinks - Ready to Eat | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 2,
            'category'      => 'Hot Snacks',
            'seo_name'      => 'hot-snacks',
            'page_title'    => 'Hot Snacks - Ready to Eat | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 2,
            'category'      => 'Nasi Kucing',
            'seo_name'      => 'nasi-kucing',
            'page_title'    => 'Nasi Kucing - Ready to Eat | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);

        Category::create([
            'parent_id'     => 2,
            'category'      => 'Nasi Bakar',
            'seo_name'      => 'nasi-bakar',
            'page_title'    => 'Nasi Bakar - Ready to Eat | Mealtime',
            'status'        => 1,
            'section_id'    => 1
        ]);*/

        Category::create([
            'category'      => 'Point',
            'seo_name'      => 'add-point',
            'page_title'    => 'Point | Mealtime',
            'status'        => 1,
            'section_id'    => 2
        ]);

    }
}