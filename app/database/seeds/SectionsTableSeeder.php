<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/23/2016
 * Time: 2:29 PM
 */

class SectionsTableSeeder extends \Seeder {
    /*
     * Run Seeder
     */
    public function run()
    {
        //delete records from table users
        DB::table('sections')->delete();

        Section::create([
            'section'   => 'Foods',
            'seo_name'  => 'food',
            'page_title'=> 'Food - Mealtime'
        ]);

        Section::create([
            'section'   => 'Subscriptions',
            'seo_name'  => 'subscriptions',
            'page_title'=> 'Subscription - Mealtime'
        ]);
    }
}