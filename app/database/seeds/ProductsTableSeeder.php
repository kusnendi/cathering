<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 3/23/2016
 * Time: 2:29 PM
 */

class ProductsTableSeeder extends \Seeder {
    /*
     * Run Seeder
     */
    public function run()
    {
        //delete records from table users
        //DB::table('products')->delete();

        Product::create([
            'product'    => 'Nasi Putih',
            'section_id' => 1,
            'category_id'=> 3,
            'description'=> 'Teksturnya gurih & lembut dimulut, membuat menu ini sedap disantap. Selain gurih, lidah balado bowl memliki rasa sedikit pedas, Apalagi ditemani dengan telur mata sapi, vegetable gulung, bawang goreng garnish, dan pickle.',
            'price'      => 7000,
            'point'      => 0,
            'status'     => 1,
            'image'      => 'products/1.jpg'
        ]);

        Product::create([
            'product'    => 'Nasi Merah',
            'section_id' => 1,
            'category_id'=> 3,
            'description'=> 'Penggemar kepiting wajib coba soka abon ayam yang dipadu dengan nasi, telur mata sapi, vegetable gulung, nori garnish, pickle, dan sambal dimsum yang menggugah selera.',
            'price'      => 8000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/2.jpg'
        ]);

        Product::create([
            'product'    => 'Mustard Chicken Burger',
            'section_id' => 1,
            'category_id'=> 3,
            'description'=> 'Potongan topokki disertai dengan irisan daging sapi dan mie, lalu dimasak dan dicampur bersama saus Korean barbeque kemudian diberi toping saus keju yang sangat lezat.',
            'price'      => 10000,
            'point'      => 8,
            'status'     => 1,
            'image'      => 'products/3.jpg'
        ]);

        Product::create([
            'product'    => 'Setengah Porsi Nasi Putih',
            'section_id' => 1,
            'category_id'=> 3,
            'description'=> 'Bumbu khas korea yang disajikan bersama wantan, vegetable gulung, topokko, tamago, dan puding kopi membuat menu bento ini sangat nikmat disantap.',
            'price'      => 4500,
            'point'      => 0,
            'status'     => 1,
            'image'      => 'products/4.jpg'
        ]);

        Product::create([
            'product'    => 'Udang Kari Singapore',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 33000,
            'point'      => 3,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Smocked Beef Chicken Layered',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Ayam Tepung Cabai Kering',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Pecak Ikan Mas',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Salad Black Mayo',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 10000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Kailan Saus Tiram',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Buncis Cah Bakso Ikan',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Tauge Cah Teri Medan',
            'section_id' => 1,
            'category_id'=> 4,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Pompom Potato',
            'section_id' => 1,
            'category_id'=> 6,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Telur Asin Sambal Terasi',
            'section_id' => 1,
            'category_id'=> 6,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Kerupuk Keriting',
            'section_id' => 1,
            'category_id'=> 6,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Steamed Siomay',
            'section_id' => 1,
            'category_id'=> 6,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Danish Raisin Bread',
            'section_id' => 1,
            'category_id'=> 5,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Pisang',
            'section_id' => 1,
            'category_id'=> 5,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Vanilla Rice Pudding',
            'section_id' => 1,
            'category_id'=> 5,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 2,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);

        Product::create([
            'product'    => 'Puding Blackcurrant Yogurt',
            'section_id' => 1,
            'category_id'=> 5,
            'description'=> 'Bumbu khas korea yang disajikan bersama vegetable gulung, topokki, dan acar membuat menu bento ini sangat nikmat disantap.',
            'price'      => 20000,
            'point'      => 1,
            'status'     => 1,
            'image'      => 'products/5.jpg'
        ]);
    }
}