<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('product');
            $table->integer('section_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->text('description');
            $table->text('ingredient');
            $table->text('calorie');
            $table->integer('price');
            $table->integer('point');
            $table->decimal('weight',8,2);
            $table->integer('length');
            $table->integer('width');
            $table->integer('height');
            $table->string('page_title');
            $table->text('meta_description');
            $table->string('meta_keyword');
            $table->integer('featured');
            $table->string('image');
            $table->integer('status');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
