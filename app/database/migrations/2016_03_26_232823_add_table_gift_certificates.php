<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableGiftCertificates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gift_certificates', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('code',30);
            $table->string('sender',50);
            $table->string('recipient',50);
            $table->integer('amount');
            $table->char('status');
            $table->string('order_ids');
            $table->string('template');
            $table->text('message');
            $table->text('products');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gift_certificates');
	}

}
