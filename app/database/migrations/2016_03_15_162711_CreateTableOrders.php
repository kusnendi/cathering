<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->integer('total');
            $table->integer('subtotal');
            $table->integer('discount');
            $table->integer('shipping_cost');
            $table->text('notes');
            $table->string('fullname');
            $table->text('shipping_address');
            $table->string('shipping_phone',15);
            $table->integer('shipping_location_id')->unsigned();
            $table->date('shipping_date');
            $table->time('shipping_time');
            $table->integer('payment_id')->unsigned();
            $table->string('ip_address',64);
            $table->char('status')->default('O');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
