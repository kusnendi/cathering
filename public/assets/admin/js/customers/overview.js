/**
 * Created by kusnendi.muhamad on 1/8/2016.
 */
$(function () {

    //Variable Declaration
    var star_date = $('#StartDate').val(),
        chartDaily, chartMonthly;

    //Init userOverview
    getuserOverview(star_date);
    getCustomerDailyMonitoring(star_date);
    getCustomerMonthlyMonitoring(star_date);

    $('#SelectDate').submit(function (event) {
        //Prevent Event from default action
        event.preventDefault();
        chartDaily.clear();
        chartDaily.destroy();
        chartMonthly.clear();
        chartMonthly.destroy();

        //Defined Variabel
        var start_date = $('#StartDate').val();

        //Call Customer Overveiw
        getuserOverview(start_date);
        //Call Chart Customer Daily Register
        getCustomerDailyMonitoring(start_date);
        //Call Overview Customer Monthly Register
        getCustomerMonthlyMonitoring(start_date);
    });

    function getuserOverview(date)
    {
        $.get('http://localhost:8080/customers?date='+date, function (result) {
            //console.log(result);
            if (result)
            {
                var userNew      = $('#uNew'),
                    userActive   = $('#uActive'),
                    userNotActive= $('#uNotActive'),
                    userTotal    = $('#uTotal');

                userNew.text(result.new);
                userActive.text(result.active);
                userNotActive.text(result.nctive);
                userTotal.text(result.total);
            }
        })
    }

    function getCustomerDailyMonitoring(date)
    {
        $.get('http://localhost:8080/customers/ajax-customer-monitoring?type=daily&date='+date, function (result) {
            if(result)
            {
                var ctx, data, options;
                Chart.defaults.global.responsive = true;
                ctx = $('#customers-daily-progress').get(0).getContext('2d');
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.label,
                    datasets: [
                        {
                            label: "Actual Sales",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.data
                        }
                    ]
                };
                chartDaily = new Chart(ctx).Line(data, options);
            }
        });
    }

    function getCustomerMonthlyMonitoring(date)
    {
        $.get('http://localhost:8080/customers/ajax-customer-monitoring?type=monthly&date='+date, function (result) {
            if(result)
            {
                var ctx, data, options;
                Chart.defaults.global.responsive = true;
                ctx = $('#customers-monthly-progress').get(0).getContext('2d');
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.label,
                    datasets: [
                        {
                            label: "Actual Sales",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.data
                        }
                    ]
                };
                chartMonthly = new Chart(ctx).Line(data, options);
            }
        });
    }

    /*
     setInterval(function(){
     // Add two random numbers for each dataset
     myLiveChart.addData([Math.random() * 100, Math.random() * 100], ++latestLabel);
     // Remove the first point so we dont just add values forever
     myLiveChart.removeData();
     }, 5000);
     */
});