$(function () {

    if ($('#ajax-cm').length)
    {
        var path_uri = window.location.pathname;
        var page = window.location.href.split('?')[1];

        if (page) loadAjax(path_uri+'/ajax?'+page);
        else loadAjax(path_uri+'/ajax');
    }

    $('.ajax').on('click', function (e) {
        /* Prevent Default */
        e.preventDefault();
    })

});

function loadAjax(url)
{
    var content = $('#ajax-cm');
    var container = $(".app-container");
    container.addClass('loader');

    content.load(url, function () {
        setTimeout(function () {
            container.removeClass('loader');
        },500);

        $('.ajax').on('click', function (e) {
            /* Prevent Default */
            e.preventDefault();
        });

        $('.pagination a').on('click', function (event) {
            event.preventDefault();

            if($(this).attr('href') != '#'){
                $("html, body").animate({scrollTop: 0}, "fast");
                loadAjax($(this).attr('href'));
                //Update Browser Url
                history.pushState(null, null, $(this).attr('href').split('/ajax')[1]);
            }
        });
    });
}

/*$(window).on('hashchange', function () {
    checkHash();
});

$(document).ready(function () {
    checkHash();
    $(document).on('click', '.pagination a', function (e) {
        getData($(this).attr('href').split('page=')[1]);
        e.preventDefault();
    })
});

function checkHash()
{
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            //alert(page);
            getData(page);
        }
    }
}

function getData(page)
{
    $.ajax({
        url: '?page=' + page,
        dataType: 'json'
    })
    .done(function (data) {
        $('#ajax-cm').html(data);
        location.hash = page;
    })
    .fail(function () {
        alert('failed to retrieved data!');
    });
}*/

$(function() {
  $(".navbar-expand-toggle").click(function() {
    $(".app-container").toggleClass("expanded");
    return $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
  });
  return $(".navbar-right-expand-toggle").click(function() {
    $(".navbar-right").toggleClass("expanded");
    return $(".navbar-right-expand-toggle").toggleClass("fa-rotate-90");
  });
});

$(function() {
  return $('select').select2();
});

$(function() {
  return $('.toggle-checkbox').bootstrapSwitch({
    size: "small"
  });
});

$(function() {
  return $('.match-height').matchHeight();
});

$(function() {
  return $('.datatable').DataTable({
    "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>'
  });
});

