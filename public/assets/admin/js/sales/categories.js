/**
 * Created by kusnendi.muhamad on 1/27/2016.
 */

$(function(){
    //Defined Variabel
    var start_date = $('#StartMonth').val();
    var ctx_month, ctx_year, data, chartMonthCategories, chartYearCategories, option_bars,
        ctx_cost_month, ctx_cost_year, chartCostMonth, chartCostYear;

    Chart.defaults.global.responsive = true;
    getOverview(start_date);
    gSalesCategories(start_date,'month');
    gSalesCategories(start_date,'year');
    gCostCategories(start_date,'month');
    gCostCategories(start_date,'year');

    $('#RequestForm').submit(function (event) {
        //Prevent Event from default action
        event.preventDefault();
        chartMonthCategories.clear();
        chartMonthCategories.destroy();
        chartYearCategories.clear();
        chartYearCategories.destroy();
        chartCostMonth.clear();
        chartCostMonth.destroy();
        chartCostYear.clear();
        chartCostYear.destroy();

        //Defined Variabel
        var start_date = $('#StartMonth').val();
        getOverview(start_date);
        gSalesCategories(start_date,'month');
        gSalesCategories(start_date,'year');
        gCostCategories(start_date,'month');
        gCostCategories(start_date,'year');
    });

    function getOverview(date)
    {
        var Sold = $('#Sold');
        var Cost = $('#Cost');

        $.get('http://localhost:8080/sales/categories?date='+date, function (result) {
            console.log(result);
            if(result)
            {
                Sold.text(result.sold);
                Cost.text(result.cost);
            }
        });
    }

    function gSalesCategories(date,interval)
    {
        if(interval == 'month')
        {
            ctx_month = document.getElementById('sales-month-categories').getContext('2d');
        }
        else
        {
            ctx_year = document.getElementById('sales-year-categories').getContext('2d');
        }
        $.get('http://localhost:8080/sales/ajax-categories/complete/?interval='+interval+'&date='+date, function (result) {

            if (result) {
                console.log(result);
                option_bars = {
                    scaleBeginAtZero: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: false,
                    barShowStroke: true,
                    barStrokeWidth: 1,
                    barValueSpacing: 5,
                    barDatasetSpacing: 3,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.category,
                    datasets: [
                        {
                            label: "Total sales by category",
                            fillColor: "rgba(26, 188, 156,0.6)",
                            strokeColor: "#1ABC9C",
                            pointColor: "#1ABC9C",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "#1ABC9C",
                            data: result.total
                        }/*
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(34, 167, 240,0.6)",
                            strokeColor: "#22A7F0",
                            pointColor: "#22A7F0",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "#22A7F0",
                            data: [28, 48, 40, 19, 86, 27, 90]
                        }
                        */
                    ]
                };

                if (interval == 'month')
                {
                    chartMonthCategories = new Chart(ctx_month).Bar(data, option_bars);
                }
                else
                {
                    chartYearCategories = new Chart(ctx_year).Bar(data, option_bars)
                }

            }
        });
    }

    function gCostCategories(date,interval)
    {
        var TopCategory = $('#TopCategory');
        var BottomCategory = $('#BottomCategory');

        if(interval == 'month')
        {
            ctx_cost_month = document.getElementById('cost-month-categories').getContext('2d');
        }
        else
        {
            ctx_cost_year = document.getElementById('cost-year-categories').getContext('2d');
        }
        $.get('http://localhost:8080/sales/ajax-categories/all/?interval='+interval+'&date='+date, function (result) {

            if (result) {
                if (interval == 'month')
                {
                    TopCategory.text(result.category[0]);
                    BottomCategory.text(result.category[result.category.length-1]);
                }
                //alert(result.category.length);
                //console.log(result);
                option_bars = {
                    scaleBeginAtZero: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: false,
                    barShowStroke: true,
                    barStrokeWidth: 1,
                    barValueSpacing: 5,
                    barDatasetSpacing: 3,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.category,
                    datasets: [
                        {
                            label: "Sales by categories cost",
                            fillColor: "rgba(34, 167, 240,0.6)",
                            strokeColor: "#22A7F0",
                            pointColor: "#22A7F0",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "#22A7F0",
                            data: result.total
                        }/*
                         {
                         label: "My Second dataset",
                         fillColor: "rgba(34, 167, 240,0.6)",
                         strokeColor: "#22A7F0",
                         pointColor: "#22A7F0",
                         pointStrokeColor: "#fff",
                         pointHighlightFill: "#fff",
                         pointHighlightStroke: "#22A7F0",
                         data: [28, 48, 40, 19, 86, 27, 90]
                         }
                         */
                    ]
                };

                if (interval == 'month')
                {
                    chartCostMonth = new Chart(ctx_cost_month).Bar(data, option_bars);
                }
                else
                {
                    chartCostYear = new Chart(ctx_cost_year).Bar(data, option_bars)
                }

            }
        });
    }

});