/**
 * Created by kusnendi.muhamad on 1/6/2016.
 */

$(function() {
    //Defined Variabel
    var start_date = $('#StartDate').val();
    var ctx, data, myLineChart, options, graphContainer;

    Chart.defaults.global.responsive = true;
    ctx = document.getElementById('sales-month-date').getContext('2d');
    getdaliySales(start_date);
    getOverviewSalesDaily(start_date);

    $('#SalesDaily').submit(function (event) {
        //Prevent Event from default action
        event.preventDefault();
        myLineChart.clear();
        myLineChart.destroy();

        //Defined Variabel
        var start_date = $('#StartDate').val();

        //Call Chart Sales Daily
        getdaliySales(start_date);
        //Call Overview Sales Daily
        getOverviewSalesDaily(start_date);
    });

    function getOverviewSalesDaily(start_date)
    {
        //Defined Variable
        var sGross = $('#saleGross');
        var sComplete  = $('#saleComplete');
        var sHigh      = $('#sHigh');
        var sLow       = $('#sLow');
        var oRequest   = $('#oRequest');
        var oOutStock  = $('#oOutStock');
        var oCancel    = $('#oCancel');
        var oComplete  = $('#oComplete');
        var totalSale  = $('#totalSales');

        $.get('http://localhost:8080/sales/ajax-daily?date='+start_date, function (result) {
            if(result)
            {
                sGross.text(Math.round(result.sales.gross));
                sComplete.text(Math.round(result.sales.complete));
                sHigh.text(Math.round(result.sales.high));
                sLow.text(Math.round(result.sales.low));
                oRequest.text((result.orders.total) ? result.orders.total : 0);
                oComplete.text((result.orders.complete) ? result.orders.complete : 0);
                oCancel.text(result.orders.cancel+result.orders.decline);
                oOutStock.text(result.orders.outstock);
                totalSale.text(currencyFormat(Math.round(result.totalSales)));
            }
            else
            {
                console.log(result.error());
            }
        })
    }

    function getdaliySales(date) {
        $.get('http://localhost:8080/sales/ajax-sales-md?date='+date, function (result) {
            if (result) {
                //var chartID = $('#graph-container').find("canvas").attr('id');
                //document.getElementById(chartID).setAttribute('id', 'sales-month-date'+date);
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.day,
                    datasets: [
                        {
                            label: "Total Sales",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: result.total
                        },
                        {
                            label: "Actual Sales",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.complete
                        }
                    ]
                };
                myLineChart = new Chart(ctx).Line(data, options);
            }
        });
    }

    function currencyFormat (num) {
        return "Rp" + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
});