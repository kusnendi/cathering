/**
 * Created by kusnendi.muhamad on 1/8/2016.
 */
$(function () {

    //Identified Variable
    var DailyRegister, MonthlyRegister, start_date = $('#StartDate').val();

    //Init merchantOverview
    getmerchantOverview(start_date);

    //Init chart Merchant Register
    chartMerchantRegister('daily', start_date);
    chartMerchantRegister('monthly', start_date);

    $('#SelectDate').submit(function (event) {
        //Prevent Event from default action
        event.preventDefault();
        DailyRegister.clear();
        DailyRegister.destroy();
        MonthlyRegister.clear();
        MonthlyRegister.destroy();

        //Defined Variabel
        var start_date = $('#StartDate').val();

        //Call Customer Overveiw
        getmerchantOverview(start_date);
        //Call Chart Customer Daily Register
        chartMerchantRegister('daily', start_date);
        //Call Overview Customer Monthly Register
        chartMerchantRegister('monthly', start_date);
    });

    function getmerchantOverview(date)
    {
        $.get('http://localhost:8080/merchants?date='+date, function (result) {
            //console.log(result);
            if (result)
            {
                var merchantNew      = $('#mNew'),
                    merchantActive   = $('#mActive'),
                    merchantNotActive= $('#mNotActive'),
                    merchantTotal    = $('#mTotal');

                merchantNew.text(result.new);
                merchantActive.text(result.active);
                merchantNotActive.text(result.nctive);
                merchantTotal.text(result.total);
            }
        })
    }

    function chartMerchantRegister(type, date)
    {
        $.get('http://localhost:8080/merchants/ajax-register-merchant?type='+type+'&date='+date, function (result) {
            if(result)
            {
                var ctx, data, options;
                Chart.defaults.global.responsive = true;

                ctx = $('#merchant-'+type+'-register').get(0).getContext('2d');
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.label,
                    datasets: [
                        {
                            label: "Daily Register",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.data
                        }
                    ]
                };

                if (type == 'daily')
                {
                    DailyRegister = new Chart(ctx).Line(data, options);
                }
                else
                {
                    MonthlyRegister = new Chart(ctx).Line(data, options);
                }

            }
        });
    }
    /*
     setInterval(function(){
     // Add two random numbers for each dataset
     myLiveChart.addData([Math.random() * 100, Math.random() * 100], ++latestLabel);
     // Remove the first point so we dont just add values forever
     myLiveChart.removeData();
     }, 5000);
     */
});