/**
 * Created by kusnendi.muhamad on 1/8/2016.
 */
$(function () {

    //Variable Declaration
    var star_date = $('#StartDate').val(),
        chartDaily, chartMonthly;

    //Init ProductOverview
    getProductOverview(star_date);
    getProductDailyMonitoring(star_date);
    getProductMonthlyMonitoring(star_date);

    $('#SelectDate').submit(function (event) {
        //Prevent Event from default action
        event.preventDefault();
        chartDaily.clear();
        chartDaily.destroy();
        chartMonthly.clear();
        chartMonthly.destroy();

        //Defined Variabel
        var start_date = $('#StartDate').val();

        //Call Product Overveiw
        getProductOverview(start_date);
        //Call Chart Sales Daily
        getProductDailyMonitoring(start_date);
        //Call Overview Sales Daily
        getProductMonthlyMonitoring(start_date);
    });

    function getProductOverview(date)
    {
        $.get('http://localhost:8080/products?date='+date, function (result) {
            if (result)
            {
                var productNew      = $('#pNew'),
                    productActive   = $('#pActive'),
                    productOutStock = $('#pOutStock'),
                    productTotal    = $('#pTotal');

                productNew.text(result.new);
                productActive.text(result.active);
                productOutStock.text(result.outstock);
                productTotal.text(result.total);
            }
        })
    }

    function getProductDailyMonitoring(date)
    {
        $.get('http://localhost:8080/products/ajax-product-monitoring?type=daily&date='+date, function (result) {
            if(result)
            {
                var ctx, data, options;
                Chart.defaults.global.responsive = true;
                ctx = $('#products-daily-progress').get(0).getContext('2d');
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.label,
                    datasets: [
                        {
                            label: "Actual Sales",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.data
                        }
                    ]
                };
                chartDaily = new Chart(ctx).Line(data, options);
            }
        });
    }

    function getProductMonthlyMonitoring(date)
    {
        $.get('http://localhost:8080/products/ajax-product-monitoring?type=monthly&date='+date, function (result) {
            if(result)
            {
                var ctx, data, options;
                Chart.defaults.global.responsive = true;
                ctx = $('#products-monthly-progress').get(0).getContext('2d');
                options = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    scaleShowHorizontalLines: true,
                    scaleShowVerticalLines: true,
                    bezierCurve: false,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
                };
                data = {
                    labels: result.label,
                    datasets: [
                        {
                            label: "Actual Sales",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: result.data
                        }
                    ]
                };
                chartMonthly = new Chart(ctx).Line(data, options);
            }
        });
    }
    /*
    setInterval(function () {
        //getProductOverview(star_date);
        console.log(star_date);
    }, 5000);

    /*
     setInterval(function(){
     // Add two random numbers for each dataset
     myLiveChart.addData([Math.random() * 100, Math.random() * 100], ++latestLabel);
     // Remove the first point so we dont just add values forever
     myLiveChart.removeData();
     }, 5000);
     */
});